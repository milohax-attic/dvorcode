/* ***************************************************************************
 *
 *   File:       Main.java
 *   Language:   Java 1.5
 *   Platform:   Java 5.0 ("Tiger") or newer
 *   OS:         n/a
 *   Authors:    [MJL]  Michael Lockhart (sinewalker)  sinewalker@gmail.com
 *
 *   Rights:
 *     Copyright © 2008, 2009 Michael James Lockhart, B.App.Comp(HONS), SCJP
 *
 *     This program is free software: you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation, either version 3 of
 *     the License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public
 *     License along with this program.  If not, see
 *     <http://www.gnu.org/licenses/>.
 *
 *   PURPOSE:
 *
 *     Main entry for Dvorcode program. This class implements a CLI to
 *     decode/encode filestreams using my "DVcode" codec. See
 *     Dvorcode.java for the details. It can optionally launch a GUI
 *     interface if that is preferred.
 *
 *   DEPENDENCIES:
 *
 *     Requires Apache Commons CLI (command-line interface
 *     API). Tested ver 1.1
 *
 *   HISTORY:
 *
 *   MJL20080722 - Created.
 *   MJL20080723 - Tweaked the CLI options.
 *   MJL20080819 - Refactor: rename package
 *                  from mjl.dvorcode to net.sf.dvorcode
 *   MJL20080820 - Code tidy -- try to fit to 80 columns
 *   MJL20080904 - Buffered output in text mode
 *               - More concise help text
 *   MJL20080909 - Removing code smells...
 *   MJL20080910 - Use an enum to represent Dvorcode codec operation (ENC/DEC)
 *   MJL20080917 - Refactor launchFilter to use BufferedWriter
 *               - Send trace output to System.err to avoid polluting output
 *               - Refactor exit code constants to an enum ExitCode
 *               - Fix issue SF 2101396 (code smell in main())
 *               - Clean up javadoc
 *   MJL20080919 - progName and Banner should be constants...
 *   MJL20080922 - More improvements to main()
 *               - some useful trace breadcrumb code
 *               - playing with assertions
 *   MJL20080923 - ExitCode enum renamed from Error, and new GENERAL_ERROR type
 *               - launchFilter(): use terminal width env var if exported
 *   MJL20080924 - Use platform-specific line separators
 *   MJL20081030 - Use Nimbus look-and-feel for "Java", if available
 *   MJL20090430 - Updated RELEASE number and BANNER, in prep for rel 0.4
 *   MJL20090504 - Formatting
 *
 *   TODO: Add a test class for net.sf.dvorcode.Main to cover text mode
 *   TODO: Add tests for GUI mode, somehow?
 */

package net.sf.dvorcode;

import org.apache.commons.cli.Parser;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import java.io.PrintWriter;
import java.util.Arrays;
import javax.swing.UIManager;


/**
 * Main-class for dvorcode transcode utility.
 * @author mike
 * @see Dvorcode
 */
public class Main {

    /** Program Banner string and copyright/warranty */
    static final String EOL = System.getProperty("line.separator");
    static final String RELEASE = "0.4";
    static final String BANNER =
   "Dvorcode, a Dvorak decoder/encoder, rel " + RELEASE + EOL
 + "Copyright (C) 2008, 2009 Michael James Lockhart, B.AppComp(HONS), SCJP"
 + EOL + EOL
 + "This is free software, licensed to you under the terms of the GNU" + EOL
 + "General Public Licence, version 3.  See http://www.gnu.org/licenses/"
 + EOL + EOL
 + "This program is distributed WITHOUT ANY WARRANTY; without even the" + EOL
 + "implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
 + EOL;

    /** The name of the program, for printing in messages */
    static final String PROGNAME = "dvorcode";

    /** Exit codes and descriptions for different exit conditions */
    private static enum ExitCode {
        /** Indicates worng options supplied */
        BAD_OPTION (1, "Argument error"),
        /** Indicates a problem processing input file */
        FILE_INPUT (2, "Problem with input file"),
        /** Indicates a problem processing output file */
        FILE_OUTPUT (3, "Problem with output file"),
        /** Indicates a problem with I/O during trans-code */
        XCODE_IO (4, "I/O Problem during trans-code"),
        /** Indicates a general, non-specific problem */
        GENERAL_ERROR (128, "Unexpected error");

        /** System.exit() code */
        private final int exitCode;

        /** ExitCode description String */
        private final String description;

        /** Initialises the exit code and description for an ExitCode.*/
        ExitCode(int code, String description) {
            this.exitCode = code;
            this.description = description;
        }

        /** @return the exit code for this ExitCode */
        int getCode() {
            return exitCode;
        }

        /** @return this ExitCode's description */
        String getDescription() {
            return description;
        }
    };

    /** when true, trace output will be printed */
    private static boolean beVerbose = false;

    /**
     * Main entry for Dvorcode program when called as a stand-alone
     * JAR.  This method will parse command-line arguments and act
     * accordingly.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Options opt = new Options();
        opt.addOption("h", "help", false, "Displays this help message");

        opt.addOption("g", "gui", false, "Use graphics mode (default)");
        opt.addOption("j", "java", false, "Use Java Look&Feel mode");

        opt.addOption("t", "text", false, "Use text mode");
        opt.addOption("f", "file", true
                     ,"File to transcode (default is stdin)");
        opt.addOption("o", "output", true
                     ,"Output to file (default is stdout)");
        opt.addOption("e", "encode", false, "Encode the input");
        opt.addOption("d", "decode", false, "Decode the input (default)");
        opt.addOption("v", "verbose", false
                     ,"Print trace information to stderr");

        Parser parser = new BasicParser();

        try {
            CommandLine cl = parser.parse(opt, args);

            if (cl.hasOption('v')) { // verbose
                beVerbose = true;
            }
            if (cl.hasOption('h')) { // help
                showUsage(opt);
            } else if (((cl.getOptions().length > 0) // some args
                        && !cl.hasOption('g') // but not graphics
                        && !cl.hasOption('j')) // and not java
                       || cl.hasOption('t')) { // text mode

                /* arguments indicate text mode operation. Figure out the
                 * text mode params and then launch the filter routine
                 */
                traceln(BANNER);

                // init params
                BufferedReader in = new BufferedReader(
                                      new InputStreamReader(System.in));
                BufferedWriter out = new BufferedWriter(
                                       new OutputStreamWriter(System.out));
                Dvorcode.Codec oper = Dvorcode.Codec.DECODE;

                if (cl.hasOption('d')) { // decode
                    oper = Dvorcode.Codec.DECODE;
                } else if (cl.hasOption('e')) { //encode
                    oper = Dvorcode.Codec.ENCODE;
                }
                if (cl.hasOption('f')) { // use an input file
                    String inFile = cl.getOptionValue('f');
                    traceln(breadCrumb(), "Using input file: \t", inFile);

                    try {
                        in = new BufferedReader(new FileReader(inFile));
                    } catch (IOException e) {
                        reportAbort(ExitCode.FILE_INPUT, inFile, e);
                    }
                }
                if (cl.hasOption('o')) { // use an output file
                    String outFile = cl.getOptionValue('o');
                    traceln(breadCrumb(), "Using output file:\t", outFile);

                    try {
                        out = new BufferedWriter(new FileWriter(outFile));
                    } catch (IOException e) {
                        reportAbort(ExitCode.FILE_OUTPUT, outFile, e);
                    }
                }

                /* params are all set. Now run the filter program
                 */
                try {
                    launchFilter(in, out, oper);
                } catch (IOException e) {
                    reportAbort(ExitCode.XCODE_IO, null, e);
                }

            } else if (cl.hasOption('j')) { // graphics with Java L&F
                try {
                    UIManager.setLookAndFeel(
                            "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
                } catch (Exception e) {
                        traceln(breadCrumb()
                                , "Exception loading Nimbus look-and-feel");
                        e.printStackTrace(System.err);
                        traceln(breadCrumb()
                                , "Falling back to Metal / default...");
                }
                launchGui();
            } else {
                assert (((0 == cl.getOptions().length) || cl.hasOption('g')) 
                        &&
                        (!cl.hasOption('j') && !cl.hasOption('t')))
                  : Arrays.asList(cl.getOptions());
                
                /* start the GUI using system look-and-feel
                 */
                try { // use host L&F
                    UIManager.setLookAndFeel(
                            UIManager.getSystemLookAndFeelClassName());
                } catch (Exception e) {
                    // whatever ... checked exceptions suck!
                    traceln(breadCrumb()
                            , "Exception loading host OS look-and-feel:");
                    e.printStackTrace(System.err);
                    traceln(breadCrumb()
                            , "Falling back to default look-and-feel...");
                }
                launchGui();
            }

        } catch (ParseException e) {
            ExitCode err = ExitCode.BAD_OPTION;
            reportError(err, null, e);
            System.err.println();

            showUsage(opt);
            abort(err);
        } catch (Exception e) {
            reportAbort(ExitCode.GENERAL_ERROR, null, e);
        }
    }

    /** prints usage information to stdout. This explains the
     *  command-line arguments.
     *
     *  @param opt the command-line options
     */
    private static void showUsage(Options opt) {
        String cmd =
                "java -jar " + PROGNAME + ".jar [option]|[option <arg>] ...";
        HelpFormatter formatter = new HelpFormatter();

        System.out.println(BANNER);
        formatter.printHelp(cmd, opt);
    }

    /** prints an exit message to System.err, which contains the
     *  message from the exception
     *
     *  @param exit the exit to report
     *  @param detail additional diagnostic detail string (may be null)
     *  @param excp an exception
     *
     *  @throws NullPointerException if parameters are null
     */
    private static void reportError
            (ExitCode exit, String detail, Exception excp) {
        if ((null == detail) || ("".equals(detail))) {
            detail = "";
        } else {
            detail = ":\t" + detail;
        }
        System.err.println(breadCrumb() + exit.getDescription() + detail);
        System.err.println(breadCrumb() + "Message: " + excp.getMessage());
    }

    /** prints an exit message to stderr and then exits the JVM with
     *  supplied exit code
     *
     *  @param exit the ExitCode enumerating the exit code
     */
    private static void abort(ExitCode exit) {
        traceln(breadCrumb(), "*** Aborting ***");
        System.exit(exit.getCode());
    }

    /** prints an exit message to System.err, which contains the
     *  message from the exception, then exits with supplied exit code
     * 
     *  @param exit the exit to report
     *  @param detail additional diagnostic detail string (may be null)
     *  @param excp an exception
     * 
     *  @throws NullPointerException if parameters are null
     * 
     *  @see #reportError(net.sf.dvorcode.Main.ExitCode,
     *                    java.lang.String, java.lang.Exception) 
     *  @see #abort(exit)
     */
    private static void reportAbort
            (ExitCode exit, String detail, Exception excp) {
        reportError(exit, detail, excp);
        abort(exit);
    }
   
    /** tests if tracing is turned on, and if so, prints the supplied
     *  message to System.err stream
     *
     *  @param message the trace message to print
     */
    static void trace(Object... message) {
        if (beVerbose) {
            StringBuilder msg = new StringBuilder();
            for (Object m : message) {
                msg.append(m.toString());
            }
            System.err.print(msg);
        }
    }

    /** tests if tracing is turned on, and if so, prints the supplied
     *  message to System.err stream, followed by a newline.
     *
     *  @param message the trace message to print
     *  @see #trace(java.lang.Object[]) 
     */
    static void traceln(Object... message) {
        if (beVerbose) {
            trace(message);
            System.err.println();
        }
    }
    
    /** @return a String containing the breadcrumb identity of the
     *          calling object, together with this program's name.
     *          Useful for debugging trace calls.
     * 
     *  @param caller the object sending the message
     */
    static String breadCrumb(Object caller) {
        return PROGNAME + ":" + Thread.currentThread().getName()
                + ":" + caller.getClass().getSimpleName() + ": ";
    }

    /** @return a String containing the breadcrumb identity of the
     *          caller (static), together with this program's name.
     *          Useful for debugging trace calls from static methods
     */
    static String breadCrumb() {
        return PROGNAME + ":" + Thread.currentThread().getName() + ":<Main>: ";
    }
    
    /** Starts a net.sf.dvorcode.MainFrame Swing frame to provide a
     *  graphic interface.
     */
    static void launchGui() {
        traceln(breadCrumb(), "launching GUI...");
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                traceln(breadCrumb(this), "running GUI...");
                MainFrame mf = new MainFrame();
                mf.setLocationByPlatform(true);
                mf.setVisible(true);
                traceln(breadCrumb(this), "GUI running");
            }
        });
    }

    /** Implements a Dvorak encode/decode filter, with progress tracing
     *  for every few lines trans-coded.
     *
     *  @param in the input stream to be filtered.
     *  @param out the stream for filtered output.
     *  @param operation the trans-code operation (encode/decode). If
     *         null, then the decode operation is chosen be default.
     *  @throws IOException if there's a problem reading/writing the
     *          streams
     *  @see Dvorcode#transcode(java.lang.String, net.sf.dvorcode.Dvorcode.Codec) 
     */
    static void launchFilter
        (BufferedReader in, BufferedWriter out, Dvorcode.Codec operation)
            throws IOException {
        if ((null != in) && (null != out)) {
            String txMsg = breadCrumb() + "Trans-coding: ";
            String txDone = " done";
            int txFreq = 8192; // trace printing frequency (in lines transcoded)
            int txMargin;  // trace wrapping margin
            try {
                txMargin = Integer.parseInt(System.getenv("COLUMNS"))-2;
            } catch (NumberFormatException e) {
                txMargin = 78;
            }
            
            if (null == operation) { // Decode by default
                operation = Dvorcode.Codec.DECODE;
            }
            String opMsg = (operation == Dvorcode.Codec.DECODE ?
                            "Operation:\t\tDECODE" : "Operation:\t\tENCODE");
            traceln(breadCrumb(), opMsg);
            
            int lines = 0;
            int column = txMsg.length()+1;

            trace(txMsg);

            PrintWriter outwr = new PrintWriter(out);

            try {
                String msg;

                while (null != (msg = in.readLine())) {
                    outwr.println(Dvorcode.transcode(msg, operation));
                    if (beVerbose && (0 == lines++ % txFreq)) {
                        trace("."); // trace a dot every txFreq lines
                        column++;
                    }
                    if (beVerbose && column > txMargin) {
                        traceln(); // wrap the trace dots
                        column = 1;
                    }
                }
            } finally {
                in.close();
                outwr.close();
                out.close();
            }
            
            if (beVerbose) {
                if ((column + txDone.length()) > txMargin) {
                    traceln(); // word(s)-wrap final message
                }
                traceln(txDone);
                traceln(breadCrumb(), "Trans-coded "+lines+" lines to output");
            }
        }
    }
}
