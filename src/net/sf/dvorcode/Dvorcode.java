/* ***************************************************************************
 *
 *   File:       Dvorcode.java
 *   Language:   Java 1.5
 *   Platform:   Java 5.0 ("Tiger") or newer
 *   OS:         n/a
 *   Authors:    [MJL]  Michael Lockhart (sinewalker)  sinewalker@gmail.com
 *
 *   Rights:
 *     Copyright © 2008, 2009 Michael James Lockhart, B.App.Comp(HONS), SCJP
 *
 *     This program is free software: you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation, either version 3 of
 *     the License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public
 *     License along with this program.  If not, see
 *     <http://www.gnu.org/licenses/>.
 *
 *   PURPOSE:
 *
 *     Class to decode/encode strings using "dvorak code".
 *     See main class documentation
 *
 *   HISTORY:
 *
 *   MJL20080721 - Created.
 *   MJL20080722 - Added CLI and GUI (seperate classes).
 *   MJL20080723 - Swap the mappings -- I'm so confused!
 *   MJL20080819 - Refactor: rename package
 *                  from mjl.dvorcode to net.sf.dvorcode
 *               - Fix transcode bug for chars not in the maps
 *                 (e.g. UTF-8)
 *   MJL20080820 - Code tidy -- try to fit to 80 columns
 *   MJL20080909 - Remove long story from javadoc -- I have a README
 *   MJL20080910 - Use an enum to represent Dvorcode codec operation (ENC/DEC)
 *               - Fix test code in main()
 *   MJL20080917 - Clean up javadoc
 *   MJL20080919 - Naming conventions for the map constants
 *   MJL20080923 - Change transcode() so that it only does the look-up once
 *   MJL20081024 - transcode(): steping through cypher instead of message
 *
 */


package net.sf.dvorcode;

import java.util.Map;
import java.util.HashMap;
import static java.lang.System.out;

/**
 * Class to transcode a string as if entered by keycap on a dvorak
 * keyboard that is marked with QWERTY keycaps. Can translate in
 * either direction by optional switch.<p/>
 *
 * <u>Use case</u>: Keep this program on a USB memory. If I'm on a
 * QWERTY keyboard and I need to know my real dvorcode Google
 * password, plug in the USB memory, start the program, type in the
 * password I've remembered ("wampafruit" say) and then copy/paste the
 * resulting dvorcode (",amlaupgcy") into the browser.<p/>
 *
 * @author mike
 */
public class Dvorcode {

    /** This maps from QWERTY keycap to Dvorak key value */
    private static final HashMap<Character,Character> DECODER
            = new HashMap<Character,Character>(33*2);
    /** This maps from Dvorak key value to QWERTY keycap */
    private static final HashMap<Character,Character> ENCODER
            = new HashMap<Character,Character>(33*2);

    /** enumerates the Dvorcode codec operations.
     *  Dvorcode can Encode or Decode depending upon the enumeration
     *  passed to it's <code>transcode(String, Codec)</code> method
     * 
     *  @see #transcode(String, Codec)
     */
    public static enum Codec {
        /** Indicates encoding operation for Dvorcode */
        ENCODE,
        /** Indicates decoding operation for Dvorcode */
        DECODE
    }

    static { // map's keycap ->  keyvalue for Dvorak key mapping (decoding)
        DECODER.put('_', '{'); DECODER.put('+', '}');
        DECODER.put('-', '['); DECODER.put('=', ']');

        DECODER.put('Q', '"'); DECODER.put('W', '<'); DECODER.put('E', '>');
        DECODER.put('q','\''); DECODER.put('w', ','); DECODER.put('e', '.');

        DECODER.put('R', 'P'); DECODER.put('T', 'Y'); DECODER.put('Y', 'F');
        DECODER.put('r', 'p'); DECODER.put('t', 'y'); DECODER.put('y', 'f');

        DECODER.put('U', 'G'); DECODER.put('I', 'C'); DECODER.put('O', 'R');
        DECODER.put('u', 'g'); DECODER.put('i', 'c'); DECODER.put('o', 'r');

        DECODER.put('P', 'L'); DECODER.put('{', '?'); DECODER.put('}', '+');
        DECODER.put('p', 'l'); DECODER.put('[', '/'); DECODER.put(']', '=');

                               DECODER.put('S', 'O'); DECODER.put('D', 'E');
                               DECODER.put('s', 'o'); DECODER.put('d', 'e');

        DECODER.put('F', 'U'); DECODER.put('G', 'I'); DECODER.put('H', 'D');
        DECODER.put('f', 'u'); DECODER.put('g', 'i'); DECODER.put('h', 'd');

        DECODER.put('J', 'H'); DECODER.put('K', 'T'); DECODER.put('L', 'N');
        DECODER.put('j', 'h'); DECODER.put('k', 't'); DECODER.put('l', 'n');

        DECODER.put(':', 'S'); DECODER.put('"', '_'); DECODER.put('Z', ':');
        DECODER.put(';', 's'); DECODER.put('\'','-'); DECODER.put('z', ';');

        DECODER.put('X', 'Q'); DECODER.put('C', 'J'); DECODER.put('V', 'K');
        DECODER.put('x', 'q'); DECODER.put('c', 'j'); DECODER.put('v', 'k');

        DECODER.put('B', 'X'); DECODER.put('N', 'B');
        DECODER.put('b', 'x'); DECODER.put('n', 'b');

        DECODER.put('<', 'W'); DECODER.put('>', 'V'); DECODER.put('?', 'Z');
        DECODER.put(',', 'w'); DECODER.put('.', 'v'); DECODER.put('/', 'z');
    }

    static { // map's keyvalue ->  keycap for Dvorak key mapping (encoding)
        // This is just a copy of the encoding map with Key and Value swapped
        for (Map.Entry<Character,Character> e : DECODER.entrySet()) {
            ENCODER.put(e.getValue(), e.getKey());
        }
    }

    /**
     * decodes or encodes the input String to/from DV encoding.
     *
     * @param cypher String to transcode
     * @param direction use the enum to indicate encode or decode
     * @return The String transcoded to/from DV encoding as requested.
     */
    public static String transcode(String cypher, Codec direction) {
        if ((null == cypher) || (null == direction)) {
            return(null);
        }

        Map<Character,Character> codec
                = (Codec.ENCODE == direction) ? ENCODER : DECODER;

        StringBuilder message = new StringBuilder(cypher);
        for (int x = 0; x < cypher.length(); x++) {
            Character y = codec.get(cypher.charAt(x));
            if (null != y) {
                message.replace(x, x+1, y.toString());
            }
        }

        return message.toString();
    }

    /**
     * Decodes the input String. This is suggar for transcode(cypher,
     * Codec.DECODE), or a synonym for transcode(cypher).
     *
     * @param cypher String to decode
     * @return the String decoded from DV encoding
     * @see #transcode(java.lang.String)  
     */
    public static String decode(String cypher) {
        return transcode (cypher);
    }

    /**
     * Encodes the input String. This is suggar for transcode(cypher,
     * Codec.ENCODE).
     *
     * @param message String to encode
     * @return the String encoded in DV encoding
     * @see #transcode(java.lang.String, net.sf.dvorcode.Dvorcode.Codec) 
     */
    public static String encode(String message) {
        return transcode (message, Codec.ENCODE);
    }

    /**
     * Decodes the input String. This is suggar for transcode(cypher,
     * Codec.DECODE).  This implements a default operation when none is
     * specified (makes the direction param optional).
     *
     * @param cypher String to decode
     * @return String decoded from DV encoding
     * @see #transcode(java.lang.String, net.sf.dvorcode.Dvorcode.Codec) 
     */
    public static String transcode(String cypher) {
        return transcode (cypher, Codec.DECODE);
    }

    /**
     * Used as a test routine only. Have a real user interface drive
     * this class instead of calling main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // test code
        out.println("Dvorak\t\tQWERTY");
        out.println("------\t\t------");
        out.println(decode("~!@#$%^&*()_+") +"\t"+ encode("~!@#$%^&*(){}"));
        out.println(decode("`1234567890-=") +"\t"+ encode("`1234567890[]\n"));

        out.println(decode("QWERTYUIOP{}|") +"\t"+ encode("\"<>PYFGCRL?+|"));
        out.println(decode("qwertyuiop[]\\")+"\t"+ encode("',.pyfgcrl/=\\\n"));

        out.println(decode(" ASDFGHJKL:\"") +"\t"+ encode(" AOEUIDHTNS_"));
        out.println(decode(" asdfghjkl;'")  +"\t"+ encode(" aoeuidhtns-\n"));

        out.println(decode("  ZXCVBNM<>?")  +"\t"+ encode("  :QJKXBMWVZ"));
        out.println(decode("  zxcvbnm,./")  +"\t"+ encode("  ;qjkxbmwvz"));

        out.println("following tests should all be true");
        out.print(decode("~!@#$%^&*()_+").equals("~!@#$%^&*(){}") + " ");
        out.print(decode("`1234567890-=").equals("`1234567890[]") + " ");
        out.print(decode("QWERTYUIOP{}|").equals("\"<>PYFGCRL?+|") + " ");
        out.print(decode("qwertyuiop[]\\").equals("',.pyfgcrl/=\\") + " ");
        out.print(decode("ASDFGHJKL:\"").equals("AOEUIDHTNS_") + " ");
        out.print(decode("asdfghjkl;'").equals("aoeuidhtns-") + " ");
        out.print(decode("ZXCVBNM<>?").equals(":QJKXBMWVZ") + " ");
        out.println(decode("zxcvbnm,./").equals(";qjkxbmwvz"));

        out.print(encode("~!@#$%^&*(){}").equals("~!@#$%^&*()_+") + " ");
        out.print(encode("`1234567890[]").equals("`1234567890-=") + " ");
        out.print(encode("\"<>PYFGCRL?+|").equals("QWERTYUIOP{}|") + " ");
        out.print(encode("',.pyfgcrl/=\\").equals("qwertyuiop[]\\") + " ");
        out.print(encode("AOEUIDHTNS_").equals("ASDFGHJKL:\"") + " ");
        out.print(encode("aoeuidhtns-").equals("asdfghjkl;'") + " ");
        out.print(encode(":QJKXBMWVZ").equals("ZXCVBNM<>?") + " ");
        out.println(encode(";qjkxbmwvz").equals("zxcvbnm,./"));
    }
}
