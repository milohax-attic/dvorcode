/* ***************************************************************************
 *
 *   File:       Dvorcode.java
 *   Language:   Java 1.5
 *   Platform:   Java 5.0 ("Tiger") or newer
 *   OS:         n/a
 *   Authors:    Michael Lockhart [MJL]
 *
 *   Rights:     Copyright © 2008 Michael James Lockhart, B.App.Comp(HONS)
 *
 *     This program is free software: you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation, either version 3 of
 *     the License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public
 *     License along with this program.  If not, see
 *     <http://www.gnu.org/licenses/>.
 *
 *   PURPOSE:
 *
 *     Class to decode/encode strings using "dvorak code".
 *     See main class documentation
 *
 *   HISTORY:
 *
 *   MJL20080721 - Created.
 *   MJL20080722 - Added CLI and GUI (seperate classes).
 *   MJL20080723 - Swap the mappings -- I'm so confused!
 *   MJL20080819 - Refactor: rename package
 *                  from mjl.dvorcode to net.sf.dvorcode
 *               - Fix transcode bug for chars not in the maps 
 *                 (e.g. UTF-8)
 *   MJL20080820 - Code tidy -- try to fit to 80 columns
 *   MJL20080909 - Remove long story from javadoc -- I have a README
 *   MJL20080910 - Use an enum to represent Dvorcode codec operation (ENC/DEC)
 *               - Fix test code in main()
 *
 */


package net.sf.dvorcode;

import java.util.Map;
import java.util.HashMap;
import static java.lang.System.out;

/**
 * Class to transcode a string as if entered by keycap on a dvorak
 * keyboard that is marked with QWERTY keycaps. Can translate in
 * either direction by optional switch.<p/>
 *
 * <u>Use case</u>: Keep this program on a USB memory. If I'm on a
 * QWERTY keyboard and I need to know my real dvorcode Google
 * password, plug in the USB memory, start the program, type in the
 * password I've remembered ("wampafruit" say) and then copy/paste the
 * resulting dvorcode (",amlaupgcy") into the browser.<p/>
 * 
 * @author mike
 */
public class Dvorcode {

    /** This maps from QWERTY keycap to Dvorak key value */
    private static final HashMap<Character,Character> dvDecMap 
            = new HashMap<Character,Character>(33*2);
    /** This maps from Dvorak key value to QWERTY keycap */
    private static final HashMap<Character,Character> dvEncMap 
            = new HashMap<Character,Character>(33*2);

    /** enumerates the Dvorcode codec operations.
     *  Dvorcode can Encode or Decode depending upon the enumeration
     *  passed to it's <code>transcode(String, Codec)</code> method
     */
    public static enum Codec {
        /** Indicates encoding operation for Dvorcode */
        ENCODE, 
        /** Indicates decoding operation for Dvorcode */
        DECODE
    };

    static { // map's keycap ->  keyvalue for Dvorak key mapping (decoding)
        dvDecMap.put('_', '{'); dvDecMap.put('+', '}');
        dvDecMap.put('-', '['); dvDecMap.put('=', ']');

        dvDecMap.put('Q', '"'); dvDecMap.put('W', '<'); dvDecMap.put('E', '>');
        dvDecMap.put('q','\''); dvDecMap.put('w', ','); dvDecMap.put('e', '.');

        dvDecMap.put('R', 'P'); dvDecMap.put('T', 'Y'); dvDecMap.put('Y', 'F');
        dvDecMap.put('r', 'p'); dvDecMap.put('t', 'y'); dvDecMap.put('y', 'f');

        dvDecMap.put('U', 'G'); dvDecMap.put('I', 'C'); dvDecMap.put('O', 'R');
        dvDecMap.put('u', 'g'); dvDecMap.put('i', 'c'); dvDecMap.put('o', 'r');

        dvDecMap.put('P', 'L'); dvDecMap.put('{', '?'); dvDecMap.put('}', '+');
        dvDecMap.put('p', 'l'); dvDecMap.put('[', '/'); dvDecMap.put(']', '=');

                                dvDecMap.put('S', 'O'); dvDecMap.put('D', 'E');
                                dvDecMap.put('s', 'o'); dvDecMap.put('d', 'e');

        dvDecMap.put('F', 'U'); dvDecMap.put('G', 'I'); dvDecMap.put('H', 'D');
        dvDecMap.put('f', 'u'); dvDecMap.put('g', 'i'); dvDecMap.put('h', 'd');

        dvDecMap.put('J', 'H'); dvDecMap.put('K', 'T'); dvDecMap.put('L', 'N');
        dvDecMap.put('j', 'h'); dvDecMap.put('k', 't'); dvDecMap.put('l', 'n');

        dvDecMap.put(':', 'S'); dvDecMap.put('"', '_'); dvDecMap.put('Z', ':');
        dvDecMap.put(';', 's'); dvDecMap.put('\'','-'); dvDecMap.put('z', ';');

        dvDecMap.put('X', 'Q'); dvDecMap.put('C', 'J'); dvDecMap.put('V', 'K');
        dvDecMap.put('x', 'q'); dvDecMap.put('c', 'j'); dvDecMap.put('v', 'k');

        dvDecMap.put('B', 'X'); dvDecMap.put('N', 'B');
        dvDecMap.put('b', 'x'); dvDecMap.put('n', 'b');

        dvDecMap.put('<', 'W'); dvDecMap.put('>', 'V'); dvDecMap.put('?', 'Z');
        dvDecMap.put(',', 'w'); dvDecMap.put('.', 'v'); dvDecMap.put('/', 'z');
    }
    
    static { // map's keyvalue ->  keycap for Dvorak key mapping (encoding)
        // This is just a copy of the encoding map with Key and Value swapped
        for (Map.Entry<Character,Character> e : dvDecMap.entrySet()) {
            dvEncMap.put(e.getValue(), e.getKey());
        }
    }
    
    /**
     * decodes or encodes the input String to/from DV encoding.
     * 
     * @param cypher String to transcode
     * @param direction use the enum to indicate encode or decode
     * @return The String transcoded to/from DV encoding as requested.
     */
    public static String transcode(String cypher, Codec direction) {
        Map codec = (Codec.ENCODE == direction) ? dvEncMap : dvDecMap;
       
        if ((null == cypher) || (null == direction)) {
            return(null);
        }
        
        StringBuilder message = new StringBuilder(cypher);
        for (int x = 0; x < message.length(); x++) {
            if (codec.containsKey(cypher.charAt(x))) {
                message.replace(x, x + 1,
                                codec.get(cypher.charAt(x)).toString());
            }
        }

        return message.toString();
    }
    
    /**
     * Decodes the input String. This is suggar for transcode(cypher,
     * Codec.DECODE), or a synonym for transcode(cypher).
     * 
     * @param cypher String to decode
     * @return the String decoded from DV encoding
     */
    public static String decode(String cypher) {
        return transcode (cypher);
    }
    
    /**
     * Encodes the input String. This is suggar for transcode(cypher,
     * Codec.ENCODE).
     * 
     * @param message String to encode
     * @return the String encoded in DV encoding
     */
    public static String encode(String message) {
        return transcode (message, Codec.ENCODE);
    }

    /**
     * Decodes the input String. This is suggar for transcode(cypher,
     * Codec.DECODE).  This implements a default operation when none is
     * specified (makes the direction param optional).
     * 
     * @param cypher String to decode
     * @return String decoded from DV encoding
     */
    public static String transcode(String cypher) {
        return transcode (cypher, Codec.DECODE); 
    }
    
    /**
     * Used as a test routine only. Have a real user interface drive
     * this class instead of calling main().
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // test code
        out.println("Dvorak\t\tQWERTY");
        out.println("------\t\t------");
        out.println(decode("~!@#$%^&*()_+") +"\t"+ encode("~!@#$%^&*(){}"));
        out.println(decode("`1234567890-=") +"\t"+ encode("`1234567890[]\n"));
                
        out.println(decode("QWERTYUIOP{}|") +"\t"+ encode("\"<>PYFGCRL?+|"));
        out.println(decode("qwertyuiop[]\\")+"\t"+ encode("',.pyfgcrl/=\\\n"));
        
        out.println(decode(" ASDFGHJKL:\"") +"\t"+ encode(" AOEUIDHTNS_"));
        out.println(decode(" asdfghjkl;'")  +"\t"+ encode(" aoeuidhtns-\n"));
        
        out.println(decode("  ZXCVBNM<>?")  +"\t"+ encode("  :QJKXBMWVZ"));
        out.println(decode("  zxcvbnm,./")  +"\t"+ encode("  ;qjkxbmwvz"));

        out.println("following tests should all be true");
        out.print(decode("~!@#$%^&*()_+").equals("~!@#$%^&*(){}") + " ");
        out.print(decode("`1234567890-=").equals("`1234567890[]") + " ");
        out.print(decode("QWERTYUIOP{}|").equals("\"<>PYFGCRL?+|") + " ");
        out.print(decode("qwertyuiop[]\\").equals("',.pyfgcrl/=\\") + " ");
        out.print(decode("ASDFGHJKL:\"").equals("AOEUIDHTNS_") + " ");
        out.print(decode("asdfghjkl;'").equals("aoeuidhtns-") + " ");
        out.print(decode("ZXCVBNM<>?").equals(":QJKXBMWVZ") + " ");
        out.println(decode("zxcvbnm,./").equals(";qjkxbmwvz"));
        
        out.print(encode("~!@#$%^&*(){}").equals("~!@#$%^&*()_+") + " ");
        out.print(encode("`1234567890[]").equals("`1234567890-=") + " ");
        out.print(encode("\"<>PYFGCRL?+|").equals("QWERTYUIOP{}|") + " ");
        out.print(encode("',.pyfgcrl/=\\").equals("qwertyuiop[]\\") + " ");
        out.print(encode("AOEUIDHTNS_").equals("ASDFGHJKL:\"") + " ");
        out.print(encode("aoeuidhtns-").equals("asdfghjkl;'") + " ");
        out.print(encode(":QJKXBMWVZ").equals("ZXCVBNM<>?") + " ");
        out.println(encode(";qjkxbmwvz").equals("zxcvbnm,./"));
    }
}
/* ***************************************************************************
 *
 *   File:       Dvorcode.java
 *   Language:   Java 1.5
  *   Platform:   Java 5.0 ("Tiger") or newer
 *   OS:         n/a
 *   Authors:    Michael Lockhart [MJL]
 *
 *   Rights:     Copyright © 2008 Michael James Lockhart, B.App.Comp(HONS)
 *
 *     This program is free software: you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation, either version 3 of
 *     the License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public
 *     License along with this program.  If not, see
 *     <http://www.gnu.org/licenses/>.
 *
 *   PURPOSE:
 *
 *     Class to decode/encode strings using "dvorak code".
 *     See main class documentation
 *
 *   HISTORY:
 *
 *   MJL20080721 - Created.
 *   MJL20080722 - Added CLI and GUI (seperate classes).
 *   MJL20080723 - Swap the mappings -- I'm so confused!
 *   MJL20080819 - Refactor: rename package
 *                  from mjl.dvorcode to net.sf.dvorcode
 *               - Fix transcode bug for chars not in the maps 
 *                 (e.g. UTF-8)
 *   MJL20080820 - Code tidy -- try to fit to 80 columns
 *   MJL20080909 - Remove long story from javadoc -- I have a README
 *   MJL20080910 - Use an enum to represent Dvorcode codec operation (ENC/DEC)
 *               - Fix test code in main()
 *
 */


package net.sf.dvorcode;

import java.util.Map;
import java.util.HashMap;
import static java.lang.System.out;

/**
 * Class to transcode a string as if entered by keycap on a dvorak
 * keyboard that is marked with QWERTY keycaps. Can translate in
 * either direction by optional switch.<p/>
 *
 * <u>Use case</u>: Keep this program on a USB memory. If I'm on a
 * QWERTY keyboard and I need to know my real dvorcode Google
 * password, plug in the USB memory, start the program, type in the
 * password I've remembered ("wampafruit" say) and then copy/paste the
 * resulting dvorcode (",amlaupgcy") into the browser.<p/>
 * 
 * @author mike
 */
public class Dvorcode {

    /** This maps from QWERTY keycap to Dvorak key value */
    private static final HashMap<Character,Character> dvDecMap 
            = new HashMap<Character,Character>(33*2);
    /** This maps from Dvorak key value to QWERTY keycap */
    private static final HashMap<Character,Character> dvEncMap 
            = new HashMap<Character,Character>(33*2);

    /** enumerates the Dvorcode codec operations.
     *  Dvorcode can Encode or Decode depending upon the enumeration
     *  passed to it's <code>transcode(String, Codec)</code> method
     */
    public static enum Codec {
        /** Indicates encoding operation for Dvorcode */
        ENCODE, 
        /** Indicates decoding operation for Dvorcode */
        DECODE
    };

    static { // map's keycap ->  keyvalue for Dvorak key mapping (decoding)
        dvDecMap.put('_', '{'); dvDecMap.put('+', '}');
        dvDecMap.put('-', '['); dvDecMap.put('=', ']');

        dvDecMap.put('Q', '"'); dvDecMap.put('W', '<'); dvDecMap.put('E', '>');
        dvDecMap.put('q','\''); dvDecMap.put('w', ','); dvDecMap.put('e', '.');

        dvDecMap.put('R', 'P'); dvDecMap.put('T', 'Y'); dvDecMap.put('Y', 'F');
        dvDecMap.put('r', 'p'); dvDecMap.put('t', 'y'); dvDecMap.put('y', 'f');

        dvDecMap.put('U', 'G'); dvDecMap.put('I', 'C'); dvDecMap.put('O', 'R');
        dvDecMap.put('u', 'g'); dvDecMap.put('i', 'c'); dvDecMap.put('o', 'r');

        dvDecMap.put('P', 'L'); dvDecMap.put('{', '?'); dvDecMap.put('}', '+');
        dvDecMap.put('p', 'l'); dvDecMap.put('[', '/'); dvDecMap.put(']', '=');

                                dvDecMap.put('S', 'O'); dvDecMap.put('D', 'E');
                                dvDecMap.put('s', 'o'); dvDecMap.put('d', 'e');

        dvDecMap.put('F', 'U'); dvDecMap.put('G', 'I'); dvDecMap.put('H', 'D');
        dvDecMap.put('f', 'u'); dvDecMap.put('g', 'i'); dvDecMap.put('h', 'd');

        dvDecMap.put('J', 'H'); dvDecMap.put('K', 'T'); dvDecMap.put('L', 'N');
        dvDecMap.put('j', 'h'); dvDecMap.put('k', 't'); dvDecMap.put('l', 'n');

        dvDecMap.put(':', 'S'); dvDecMap.put('"', '_'); dvDecMap.put('Z', ':');
        dvDecMap.put(';', 's'); dvDecMap.put('\'','-'); dvDecMap.put('z', ';');

        dvDecMap.put('X', 'Q'); dvDecMap.put('C', 'J'); dvDecMap.put('V', 'K');
        dvDecMap.put('x', 'q'); dvDecMap.put('c', 'j'); dvDecMap.put('v', 'k');

        dvDecMap.put('B', 'X'); dvDecMap.put('N', 'B');
        dvDecMap.put('b', 'x'); dvDecMap.put('n', 'b');

        dvDecMap.put('<', 'W'); dvDecMap.put('>', 'V'); dvDecMap.put('?', 'Z');
        dvDecMap.put(',', 'w'); dvDecMap.put('.', 'v'); dvDecMap.put('/', 'z');
    }
    
    static { // map's keyvalue ->  keycap for Dvorak key mapping (encoding)
        // This is just a copy of the encoding map with Key and Value swapped
        for (Map.Entry<Character,Character> e : dvDecMap.entrySet()) {
            dvEncMap.put(e.getValue(), e.getKey());
        }
    }
    
    /**
     * decodes or encodes the input String to/from DV encoding.
     * 
     * @param cypher String to transcode
     * @param direction use the enum to indicate encode or decode
     * @return The String transcoded to/from DV encoding as requested.
     */
    public static String transcode(String cypher, Codec direction) {
        Map codec = (Codec.ENCODE == direction) ? dvEncMap : dvDecMap;
       
        if ((null == cypher) || (null == direction)) {
            return(null);
        }
        
        StringBuilder message = new StringBuilder(cypher);
        for (int x = 0; x < message.length(); x++) {
            if (codec.containsKey(cypher.charAt(x))) {
                message.replace(x, x + 1,
                                codec.get(cypher.charAt(x)).toString());
            }
        }

        return message.toString();
    }
    
    /**
     * Decodes the input String. This is suggar for transcode(cypher,
     * Codec.DECODE), or a synonym for transcode(cypher).
     * 
     * @param cypher String to decode
     * @return the String decoded from DV encoding
     */
    public static String decode(String cypher) {
        return transcode (cypher);
    }
    
    /**
     * Encodes the input String. This is suggar for transcode(cypher,
     * Codec.ENCODE).
     * 
     * @param message String to encode
     * @return the String encoded in DV encoding
     */
    public static String encode(String message) {
        return transcode (message, Codec.ENCODE);
    }

    /**
     * Decodes the input String. This is suggar for transcode(cypher,
     * Codec.DECODE).  This implements a default operation when none is
     * specified (makes the direction param optional).
     * 
     * @param cypher String to decode
     * @return String decoded from DV encoding
     */
    public static String transcode(String cypher) {
        return transcode (cypher, Codec.DECODE); 
    }
    
    /**
     * Used as a test routine only. Have a real user interface drive
     * this class instead of calling main().
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // test code
        out.println("Dvorak\t\tQWERTY");
        out.println("------\t\t------");
        out.println(decode("~!@#$%^&*()_+") +"\t"+ encode("~!@#$%^&*(){}"));
        out.println(decode("`1234567890-=") +"\t"+ encode("`1234567890[]\n"));
                
        out.println(decode("QWERTYUIOP{}|") +"\t"+ encode("\"<>PYFGCRL?+|"));
        out.println(decode("qwertyuiop[]\\")+"\t"+ encode("',.pyfgcrl/=\\\n"));
        
        out.println(decode(" ASDFGHJKL:\"") +"\t"+ encode(" AOEUIDHTNS_"));
        out.println(decode(" asdfghjkl;'")  +"\t"+ encode(" aoeuidhtns-\n"));
        
        out.println(decode("  ZXCVBNM<>?")  +"\t"+ encode("  :QJKXBMWVZ"));
        out.println(decode("  zxcvbnm,./")  +"\t"+ encode("  ;qjkxbmwvz"));

        out.println("following tests should all be true");
        out.print(decode("~!@#$%^&*()_+").equals("~!@#$%^&*(){}") + " ");
        out.print(decode("`1234567890-=").equals("`1234567890[]") + " ");
        out.print(decode("QWERTYUIOP{}|").equals("\"<>PYFGCRL?+|") + " ");
        out.print(decode("qwertyuiop[]\\").equals("',.pyfgcrl/=\\") + " ");
        out.print(decode("ASDFGHJKL:\"").equals("AOEUIDHTNS_") + " ");
        out.print(decode("asdfghjkl;'").equals("aoeuidhtns-") + " ");
        out.print(decode("ZXCVBNM<>?").equals(":QJKXBMWVZ") + " ");
        out.println(decode("zxcvbnm,./").equals(";qjkxbmwvz"));
        
        out.print(encode("~!@#$%^&*(){}").equals("~!@#$%^&*()_+") + " ");
        out.print(encode("`1234567890[]").equals("`1234567890-=") + " ");
        out.print(encode("\"<>PYFGCRL?+|").equals("QWERTYUIOP{}|") + " ");
        out.print(encode("',.pyfgcrl/=\\").equals("qwertyuiop[]\\") + " ");
        out.print(encode("AOEUIDHTNS_").equals("ASDFGHJKL:\"") + " ");
        out.print(encode("aoeuidhtns-").equals("asdfghjkl;'") + " ");
        out.print(encode(":QJKXBMWVZ").equals("ZXCVBNM<>?") + " ");
        out.println(encode(";qjkxbmwvz").equals("zxcvbnm,./"));
    }
}
/* ***************************************************************************
 *
 *   File:       Dvorcode.java
 *   Language:   Java 1.5
  *   Platform:   Java 5.0 ("Tiger") or newer
 *   OS:         n/a
 *   Authors:    Michael Lockhart [MJL]
 *
 *   Rights:     Copyright © 2008 Michael James Lockhart, B.App.Comp(HONS)
 *
 *     This program is free software: you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation, either version 3 of
 *     the License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public
 *     License along with this program.  If not, see
 *     <http://www.gnu.org/licenses/>.
 *
 *   PURPOSE:
 *
 *     Class to decode/encode strings using "dvorak code".
 *     See main class documentation
 *
 *   HISTORY:
 *
 *   MJL20080721 - Created.
 *   MJL20080722 - Added CLI and GUI (seperate classes).
 *   MJL20080723 - Swap the mappings -- I'm so confused!
 *   MJL20080819 - Refactor: rename package
 *                  from mjl.dvorcode to net.sf.dvorcode
 *               - Fix transcode bug for chars not in the maps 
 *                 (e.g. UTF-8)
 *   MJL20080820 - Code tidy -- try to fit to 80 columns
 *   MJL20080909 - Remove long story from javadoc -- I have a README
 *   MJL20080910 - Use an enum to represent Dvorcode codec operation (ENC/DEC)
 *               - Fix test code in main()
 *
 */


package net.sf.dvorcode;

import java.util.Map;
import java.util.HashMap;
import static java.lang.System.out;

/**
 * Class to transcode a string as if entered by keycap on a dvorak
 * keyboard that is marked with QWERTY keycaps. Can translate in
 * either direction by optional switch.<p/>
 *
 * <u>Use case</u>: Keep this program on a USB memory. If I'm on a
 * QWERTY keyboard and I need to know my real dvorcode Google
 * password, plug in the USB memory, start the program, type in the
 * password I've remembered ("wampafruit" say) and then copy/paste the
 * resulting dvorcode (",amlaupgcy") into the browser.<p/>
 * 
 * @author mike
 */
public class Dvorcode {

    /** This maps from QWERTY keycap to Dvorak key value */
    private static final HashMap<Character,Character> dvDecMap 
            = new HashMap<Character,Character>(33*2);
    /** This maps from Dvorak key value to QWERTY keycap */
    private static final HashMap<Character,Character> dvEncMap 
            = new HashMap<Character,Character>(33*2);

    /** enumerates the Dvorcode codec operations.
     *  Dvorcode can Encode or Decode depending upon the enumeration
     *  passed to it's <code>transcode(String, Codec)</code> method
     */
    public static enum Codec {
        /** Indicates encoding operation for Dvorcode */
        ENCODE, 
        /** Indicates decoding operation for Dvorcode */
        DECODE
    };

    static { // map's keycap ->  keyvalue for Dvorak key mapping (decoding)
        dvDecMap.put('_', '{'); dvDecMap.put('+', '}');
        dvDecMap.put('-', '['); dvDecMap.put('=', ']');

        dvDecMap.put('Q', '"'); dvDecMap.put('W', '<'); dvDecMap.put('E', '>');
        dvDecMap.put('q','\''); dvDecMap.put('w', ','); dvDecMap.put('e', '.');

        dvDecMap.put('R', 'P'); dvDecMap.put('T', 'Y'); dvDecMap.put('Y', 'F');
        dvDecMap.put('r', 'p'); dvDecMap.put('t', 'y'); dvDecMap.put('y', 'f');

        dvDecMap.put('U', 'G'); dvDecMap.put('I', 'C'); dvDecMap.put('O', 'R');
        dvDecMap.put('u', 'g'); dvDecMap.put('i', 'c'); dvDecMap.put('o', 'r');

        dvDecMap.put('P', 'L'); dvDecMap.put('{', '?'); dvDecMap.put('}', '+');
        dvDecMap.put('p', 'l'); dvDecMap.put('[', '/'); dvDecMap.put(']', '=');

                                dvDecMap.put('S', 'O'); dvDecMap.put('D', 'E');
                                dvDecMap.put('s', 'o'); dvDecMap.put('d', 'e');

        dvDecMap.put('F', 'U'); dvDecMap.put('G', 'I'); dvDecMap.put('H', 'D');
        dvDecMap.put('f', 'u'); dvDecMap.put('g', 'i'); dvDecMap.put('h', 'd');

        dvDecMap.put('J', 'H'); dvDecMap.put('K', 'T'); dvDecMap.put('L', 'N');
        dvDecMap.put('j', 'h'); dvDecMap.put('k', 't'); dvDecMap.put('l', 'n');

        dvDecMap.put(':', 'S'); dvDecMap.put('"', '_'); dvDecMap.put('Z', ':');
        dvDecMap.put(';', 's'); dvDecMap.put('\'','-'); dvDecMap.put('z', ';');

        dvDecMap.put('X', 'Q'); dvDecMap.put('C', 'J'); dvDecMap.put('V', 'K');
        dvDecMap.put('x', 'q'); dvDecMap.put('c', 'j'); dvDecMap.put('v', 'k');

        dvDecMap.put('B', 'X'); dvDecMap.put('N', 'B');
        dvDecMap.put('b', 'x'); dvDecMap.put('n', 'b');

        dvDecMap.put('<', 'W'); dvDecMap.put('>', 'V'); dvDecMap.put('?', 'Z');
        dvDecMap.put(',', 'w'); dvDecMap.put('.', 'v'); dvDecMap.put('/', 'z');
    }
    
    static { // map's keyvalue ->  keycap for Dvorak key mapping (encoding)
        // This is just a copy of the encoding map with Key and Value swapped
        for (Map.Entry<Character,Character> e : dvDecMap.entrySet()) {
            dvEncMap.put(e.getValue(), e.getKey());
        }
    }
    
    /**
     * decodes or encodes the input String to/from DV encoding.
     * 
     * @param cypher String to transcode
     * @param direction use the enum to indicate encode or decode
     * @return The String transcoded to/from DV encoding as requested.
     */
    public static String transcode(String cypher, Codec direction) {
        Map codec = (Codec.ENCODE == direction) ? dvEncMap : dvDecMap;
       
        if ((null == cypher) || (null == direction)) {
            return(null);
        }
        
        StringBuilder message = new StringBuilder(cypher);
        for (int x = 0; x < message.length(); x++) {
            if (codec.containsKey(cypher.charAt(x))) {
                message.replace(x, x + 1,
                                codec.get(cypher.charAt(x)).toString());
            }
        }

        return message.toString();
    }
    
    /**
     * Decodes the input String. This is suggar for transcode(cypher,
     * Codec.DECODE), or a synonym for transcode(cypher).
     * 
     * @param cypher String to decode
     * @return the String decoded from DV encoding
     */
    public static String decode(String cypher) {
        return transcode (cypher);
    }
    
    /**
     * Encodes the input String. This is suggar for transcode(cypher,
     * Codec.ENCODE).
     * 
     * @param message String to encode
     * @return the String encoded in DV encoding
     */
    public static String encode(String message) {
        return transcode (message, Codec.ENCODE);
    }

    /**
     * Decodes the input String. This is suggar for transcode(cypher,
     * Codec.DECODE).  This implements a default operation when none is
     * specified (makes the direction param optional).
     * 
     * @param cypher String to decode
     * @return String decoded from DV encoding
     */
    public static String transcode(String cypher) {
        return transcode (cypher, Codec.DECODE); 
    }
    
    /**
     * Used as a test routine only. Have a real user interface drive
     * this class instead of calling main().
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // test code
        out.println("Dvorak\t\tQWERTY");
        out.println("------\t\t------");
        out.println(decode("~!@#$%^&*()_+") +"\t"+ encode("~!@#$%^&*(){}"));
        out.println(decode("`1234567890-=") +"\t"+ encode("`1234567890[]\n"));
                
        out.println(decode("QWERTYUIOP{}|") +"\t"+ encode("\"<>PYFGCRL?+|"));
        out.println(decode("qwertyuiop[]\\")+"\t"+ encode("',.pyfgcrl/=\\\n"));
        
        out.println(decode(" ASDFGHJKL:\"") +"\t"+ encode(" AOEUIDHTNS_"));
        out.println(decode(" asdfghjkl;'")  +"\t"+ encode(" aoeuidhtns-\n"));
        
        out.println(decode("  ZXCVBNM<>?")  +"\t"+ encode("  :QJKXBMWVZ"));
        out.println(decode("  zxcvbnm,./")  +"\t"+ encode("  ;qjkxbmwvz"));

        out.println("following tests should all be true");
        out.print(decode("~!@#$%^&*()_+").equals("~!@#$%^&*(){}") + " ");
        out.print(decode("`1234567890-=").equals("`1234567890[]") + " ");
        out.print(decode("QWERTYUIOP{}|").equals("\"<>PYFGCRL?+|") + " ");
        out.print(decode("qwertyuiop[]\\").equals("',.pyfgcrl/=\\") + " ");
        out.print(decode("ASDFGHJKL:\"").equals("AOEUIDHTNS_") + " ");
        out.print(decode("asdfghjkl;'").equals("aoeuidhtns-") + " ");
        out.print(decode("ZXCVBNM<>?").equals(":QJKXBMWVZ") + " ");
        out.println(decode("zxcvbnm,./").equals(";qjkxbmwvz"));
        
        out.print(encode("~!@#$%^&*(){}").equals("~!@#$%^&*()_+") + " ");
        out.print(encode("`1234567890[]").equals("`1234567890-=") + " ");
        out.print(encode("\"<>PYFGCRL?+|").equals("QWERTYUIOP{}|") + " ");
        out.print(encode("',.pyfgcrl/=\\").equals("qwertyuiop[]\\") + " ");
        out.print(encode("AOEUIDHTNS_").equals("ASDFGHJKL:\"") + " ");
        out.print(encode("aoeuidhtns-").equals("asdfghjkl;'") + " ");
        out.print(encode(":QJKXBMWVZ").equals("ZXCVBNM<>?") + " ");
        out.println(encode(";qjkxbmwvz").equals("zxcvbnm,./"));
    }
}
