[* ***************************************************************************
 *
 *   YgpdZ       H.soishdeca.a
 *   PalufaudZ   Ca.a 1e5
 *   RpakysomZ   Ca.a 5e0 (QKgudoQ) so ld,do
 *   S:Z         l[a
 *   Afkjso;Z    Mgijadp Psivjaok -MCP=
 *
 *   Ogujk;Z     Isrtogujk © 2008 Mgijadp Camd; Psivjaokw NeArreIsmr(JSL:)
 *
 *     Kjg; rosuoam g; yodd ;syk,aodZ tsf ial odhg;kognfkd gk alh[so
 *     mshgyt gk flhdo kjd kdom; sy kjd ULF Udldoap Rfnpgi Pgidl;d a;
 *     rfnpg;jdh nt kjd Yodd :syk,aod Ysflhakgslw dgkjdo .do;gsl 3 sy
 *     kjd Pgidl;dw so (ak tsfo srkgsl) alt pakdo .do;gsle
 *
 *     Kjg; rosuoam g; hg;kognfkdh gl kjd jsrd kjak gk ,gpp nd f;dyfpw
 *     nfk <GKJSFK ALT <AOOALKTz ,gkjsfk d.dl kjd gmrpgdh ,aooalkt sy
 *     MDOIJALKANGPGKT so YGKLD:: YSO A RAOKGIFPAO RFORS:De  :dd kjd
 *     ULF Udldoap Rfnpgi Pgidl;d yso msod hdkagp;e
 *
 *     Tsf ;jsfph ja.d odidg.dh a isrt sy kjd ULF Udldoap Rfnpgi
 *     Pgidl;d apslu ,gkj kjg; rosuoame  Gy lskw ;dd
 *     WjkkrZ[[,,,eulfesou[pgidl;d;[Ee
 *
 *   RFORS:DZ
 *
 *     Ipa;; ks hdishd[dlishd ;koglu; f;glu Qh.soav ishdQe
 *     :dd magl ipa;; hsifmdlkakgsl
 *
 *   JG:KSOTZ
 *
 *   MCP20080721 ' Iodakdhe
 *   MCP20080722 ' Ahhdh IPG alh UFG (;drdoakd ipa;;d;)e
 *   MCP20080723 ' :,ar kjd marrglu; '' Gqm ;s islyf;dh!
 *   MCP20080819 ' OdyaiksoZ odlamd raivaud
 *                  yosm mcpeh.soishd ks ldke;yeh.soishd
 *               ' Ygb koal;ishd nfu yso ijao; lsk gl kjd mar; 
 *                 (deue FKY'8)
 *   MCP20080820 ' Ishd kght '' kot ks ygk ks 80 ispfml;
 *   MCP20080909 ' Odms.d pslu ;ksot yosm ca.ahsi '' G ja.d a ODAHMD
 *   MCP20080910 ' F;d al dlfm ks odrod;dlk H.soishd ishdi srdoakgsl (DLI[HDI)
 *               ' Ygb kd;k ishd gl magl()
 *
 *[


raivaud ldke;yeh.soishdz

gmrsok ca.aefkgpeMarz
gmrsok ca.aefkgpeJa;jMarz
gmrsok ;kakgi ca.aepalue:t;kdmesfkz

[**
 * Ipa;; ks koal;ishd a ;koglu a; gy dlkdodh nt vdtiar sl a h.soav
 * vdtnsaoh kjak g; maovdh ,gkj X<DOKT vdtiar;e Ial koal;pakd gl
 * dgkjdo hgodikgsl nt srkgslap ;,gkijeWr[E
 *
 * WfEF;d ia;dW[fEZ Vddr kjg; rosuoam sl a F:N mdmsote Gy Gqm sl a
 * X<DOKT vdtnsaoh alh G lddh ks vls, mt odap h.soishd Ussupd
 * ra;;,sohw rpfu gl kjd F:N mdmsotw ;kaok kjd rosuoamw ktrd gl kjd
 * ra;;,soh Gq.d odmdmndodh (Q,amrayofgkQ ;at) alh kjdl isrt[ra;kd kjd
 * od;fpkglu h.soishd (QwampafruitQ) glks kjd nos,;doeWr[E
 * 
 * @afkjso mgvd
 *[
rfnpgi ipa;; H.soishd _

    [** Kjg; mar; yosm X<DOKT vdtiar ks H.soav vdt .apfd *[
    rog.akd ;kakgi yglap Ja;jMarWIjaoaikdowIjaoaikdoE h.HdiMar 
            ] ld, Ja;jMarWIjaoaikdowIjaoaikdoE(33*2)z
    [** Kjg; mar; yosm H.soav vdt .apfd ks X<DOKT vdtiar *[
    rog.akd ;kakgi yglap Ja;jMarWIjaoaikdowIjaoaikdoE h.DliMar 
            ] ld, Ja;jMarWIjaoaikdowIjaoaikdoE(33*2)z

    [** dlfmdoakd; kjd H.soishd ishdi srdoakgsl;e
     *  H.soishd ial Dlishd so Hdishd hdrdlhglu frsl kjd dlfmdoakgsl
     *  ra;;dh ks gkq; WishdEkoal;ishd(:kogluw Ishdi)W[ishdE mdkjsh
     *[
    rfnpgi ;kakgi dlfm Ishdi _
        [** Glhgiakd; dlishglu srdoakgsl yso H.soishd *[
        DLISHDw 
        [** Glhgiakd; hdishglu srdoakgsl yso H.soishd *[
        HDISHD
    +z

    ;kakgi _ [[ marq; vdtiar 'E  vdt.apfd yso H.soav vdt marrglu (hdishglu)
        h.HdiMarerfk(q"qw q_q)z h.HdiMarerfk(q}qw q+q)z
        h.HdiMarerfk(q'qw q-q)z h.HdiMarerfk(q]qw q=q)z

        h.HdiMarerfk(qXqw qQq)z h.HdiMarerfk(q<qw qWq)z h.HdiMarerfk(qDqw qEq)z
        h.HdiMarerfk(qxqwq\qq)z h.HdiMarerfk(q,qw qwq)z h.HdiMarerfk(qdqw qeq)z

        h.HdiMarerfk(qOqw qRq)z h.HdiMarerfk(qKqw qTq)z h.HdiMarerfk(qTqw qYq)z
        h.HdiMarerfk(qoqw qrq)z h.HdiMarerfk(qkqw qtq)z h.HdiMarerfk(qtqw qyq)z

        h.HdiMarerfk(qFqw qUq)z h.HdiMarerfk(qGqw qIq)z h.HdiMarerfk(qSqw qOq)z
        h.HdiMarerfk(qfqw quq)z h.HdiMarerfk(qgqw qiq)z h.HdiMarerfk(qsqw qoq)z

        h.HdiMarerfk(qRqw qPq)z h.HdiMarerfk(q_qw q{q)z h.HdiMarerfk(q+qw q}q)z
        h.HdiMarerfk(qrqw qpq)z h.HdiMarerfk(q-qw q[q)z h.HdiMarerfk(q=qw q]q)z

                                h.HdiMarerfk(q:qw qSq)z h.HdiMarerfk(qHqw qDq)z
                                h.HdiMarerfk(q;qw qsq)z h.HdiMarerfk(qhqw qdq)z

        h.HdiMarerfk(qYqw qFq)z h.HdiMarerfk(qUqw qGq)z h.HdiMarerfk(qJqw qHq)z
        h.HdiMarerfk(qyqw qfq)z h.HdiMarerfk(quqw qgq)z h.HdiMarerfk(qjqw qhq)z

        h.HdiMarerfk(qCqw qJq)z h.HdiMarerfk(qVqw qKq)z h.HdiMarerfk(qPqw qLq)z
        h.HdiMarerfk(qcqw qjq)z h.HdiMarerfk(qvqw qkq)z h.HdiMarerfk(qpqw qlq)z

        h.HdiMarerfk(qZqw q:q)z h.HdiMarerfk(qQqw q"q)z h.HdiMarerfk(q?qw qZq)z
        h.HdiMarerfk(qzqw q;q)z h.HdiMarerfk(q\qqwq'q)z h.HdiMarerfk(q/qw qzq)z

        h.HdiMarerfk(qBqw qXq)z h.HdiMarerfk(qIqw qCq)z h.HdiMarerfk(q>qw qVq)z
        h.HdiMarerfk(qbqw qxq)z h.HdiMarerfk(qiqw qcq)z h.HdiMarerfk(q.qw qvq)z

        h.HdiMarerfk(qNqw qBq)z h.HdiMarerfk(qLqw qNq)z
        h.HdiMarerfk(qnqw qbq)z h.HdiMarerfk(qlqw qnq)z

        h.HdiMarerfk(qWqw q<q)z h.HdiMarerfk(qEqw q>q)z h.HdiMarerfk(q{qw q?q)z
        h.HdiMarerfk(qwqw q,q)z h.HdiMarerfk(qeqw q.q)z h.HdiMarerfk(q[qw q/q)z
    +
    
    ;kakgi _ [[ marq; vdt.apfd 'E  vdtiar yso H.soav vdt marrglu (dlishglu)
        [[ Kjg; g; cf;k a isrt sy kjd dlishglu mar ,gkj Vdt alh >apfd ;,arrdh
        yso (MareDlkotWIjaoaikdowIjaoaikdoE d Z h.HdiMaredlkot:dk()) _
            h.DliMarerfk(deudk>apfd()w deudkVdt())z
        +
    +
    
    [**
     * hdishd; so dlishd; kjd glrfk :koglu ks[yosm H> dlishglue
     * 
     * @raoam itrjdo :koglu ks koal;ishd
     * @raoam hgodikgsl f;d kjd dlfm ks glhgiakd dlishd so hdishd
     * @odkfol Kjd :koglu koal;ishdh ks[yosm H> dlishglu a; odxfd;kdhe
     *[
    rfnpgi ;kakgi :koglu koal;ishd(:koglu itrjdow Ishdi hgodikgsl) _
        Mar ishdi ] (IshdieDLISHD ]] hgodikgsl) { h.DliMar Z h.HdiMarz
       
        gy ((lfpp ]] itrjdo) || (lfpp ]] hgodikgsl)) _
            odkfol(lfpp)z
        +
        
        :kogluNfgphdo md;;aud ] ld, :kogluNfgphdo(itrjdo)z
        yso (glk b ] 0z b W md;;audepdlukj()z b}}) _
            gy (ishdieislkagl;Vdt(itrjdoeijaoAk(b))) _
                md;;audeodrpaid(bw b } 1w
                                ishdieudk(itrjdoeijaoAk(b))eks:koglu())z
            +
        +

        odkfol md;;audeks:koglu()z
    +
    
    [**
     * Hdishd; kjd glrfk :koglue Kjg; g; ;fuuao yso koal;ishd(itrjdow
     * IshdieHDISHD)w so a ;tlsltm yso koal;ishd(itrjdo)e
     * 
     * @raoam itrjdo :koglu ks hdishd
     * @odkfol kjd :koglu hdishdh yosm H> dlishglu
     *[
    rfnpgi ;kakgi :koglu hdishd(:koglu itrjdo) _
        odkfol koal;ishd (itrjdo)z
    +
    
    [**
     * Dlishd; kjd glrfk :koglue Kjg; g; ;fuuao yso koal;ishd(itrjdow
     * IshdieDLISHD)e
     * 
     * @raoam md;;aud :koglu ks dlishd
     * @odkfol kjd :koglu dlishdh gl H> dlishglu
     *[
    rfnpgi ;kakgi :koglu dlishd(:koglu md;;aud) _
        odkfol koal;ishd (md;;audw IshdieDLISHD)z
    +

    [**
     * Hdishd; kjd glrfk :koglue Kjg; g; ;fuuao yso koal;ishd(itrjdow
     * IshdieHDISHD)e  Kjg; gmrpdmdlk; a hdyafpk srdoakgsl ,jdl lsld g;
     * ;rdigygdh (mavd; kjd hgodikgsl raoam srkgslap)e
     * 
     * @raoam itrjdo :koglu ks hdishd
     * @odkfol :koglu hdishdh yosm H> dlishglu
     *[
    rfnpgi ;kakgi :koglu koal;ishd(:koglu itrjdo) _
        odkfol koal;ishd (itrjdow IshdieHDISHD)z 
    +
    
    [**
     * F;dh a; a kd;k osfkgld slpte Ja.d a odap f;do glkdoyaid hog.d
     * kjg; ipa;; gl;kdah sy iappglu magl()e
     * 
     * @raoam aou; kjd ismmalh pgld aoufmdlk;
     *[
    rfnpgi ;kakgi .sgh magl(:koglu-= aou;) _
        [[ kd;k ishd
        sfkeroglkpl(QH.soav\k\kX<DOKTQ)z
        sfkeroglkpl(Q''''''\k\k''''''Q)z
        sfkeroglkpl(hdishd(Q~!@#$%^&*()"}Q) }Q\kQ} dlishd(Q~!@#$%^&*()_+Q))z
        sfkeroglkpl(hdishd(Q`1234567890']Q) }Q\kQ} dlishd(Q`1234567890-=\lQ))z
                
        sfkeroglkpl(hdishd(QX<DOKTFGSR_+|Q) }Q\kQ} dlishd(Q\QWERTYUIOP{}|Q))z
        sfkeroglkpl(hdishd(Qx,doktfgsr-=\\Q)}Q\kQ} dlishd(Qqwertyuiop[]\\\lQ))z
        
        sfkeroglkpl(hdishd(Q A:HYUJCVPZ\QQ) }Q\kQ} dlishd(Q ASDFGHJKL:"Q))z
        sfkeroglkpl(hdishd(Q a;hyujcvpzqQ)  }Q\kQ} dlishd(Q asdfghjkl;'\lQ))z
        
        sfkeroglkpl(hdishd(Q  ?BI>NLMWE{Q)  }Q\kQ} dlishd(Q  ZXCVBNM<>?Q))z
        sfkeroglkpl(hdishd(Q  /bi.nlmwe[Q)  }Q\kQ} dlishd(Q  zxcvbnm,./Q))z

        sfkeroglkpl(Qyspps,glu kd;k; ;jsfph app nd kofdQ)z
        sfkeroglk(hdishd(Q~!@#$%^&*()"}Q)edxfap;(Q~!@#$%^&*()_+Q) } Q Q)z
        sfkeroglk(hdishd(Q`1234567890']Q)edxfap;(Q`1234567890-=Q) } Q Q)z
        sfkeroglk(hdishd(QX<DOKTFGSR_+|Q)edxfap;(Q\QWERTYUIOP{}|Q) } Q Q)z
        sfkeroglk(hdishd(Qx,doktfgsr-=\\Q)edxfap;(Qqwertyuiop[]\\Q) } Q Q)z
        sfkeroglk(hdishd(QA:HYUJCVPZ\QQ)edxfap;(QASDFGHJKL:"Q) } Q Q)z
        sfkeroglk(hdishd(Qa;hyujcvpzqQ)edxfap;(Qasdfghjkl;'Q) } Q Q)z
        sfkeroglk(hdishd(Q?BI>NLMWE{Q)edxfap;(QZXCVBNM<>?Q) } Q Q)z
        sfkeroglkpl(hdishd(Q/bi.nlmwe[Q)edxfap;(Qzxcvbnm,./Q))z
        
        sfkeroglk(dlishd(Q~!@#$%^&*()_+Q)edxfap;(Q~!@#$%^&*()"}Q) } Q Q)z
        sfkeroglk(dlishd(Q`1234567890-=Q)edxfap;(Q`1234567890']Q) } Q Q)z
        sfkeroglk(dlishd(Q\QWERTYUIOP{}|Q)edxfap;(QX<DOKTFGSR_+|Q) } Q Q)z
        sfkeroglk(dlishd(Qqwertyuiop[]\\Q)edxfap;(Qx,doktfgsr-=\\Q) } Q Q)z
        sfkeroglk(dlishd(QASDFGHJKL:"Q)edxfap;(QA:HYUJCVPZ\QQ) } Q Q)z
        sfkeroglk(dlishd(Qasdfghjkl;'Q)edxfap;(Qa;hyujcvpzqQ) } Q Q)z
        sfkeroglk(dlishd(QZXCVBNM<>?Q)edxfap;(Q?BI>NLMWE{Q) } Q Q)z
        sfkeroglkpl(dlishd(Qzxcvbnm,./Q)edxfap;(Q/bi.nlmwe[Q))z
    +
+
[* ***************************************************************************
 *
 *   YgpdZ       H.soishdeca.a
 *   PalufaudZ   Ca.a 1e5
  *   RpakysomZ   Ca.a 5e0 (QKgudoQ) so ld,do
 *   S:Z         l[a
 *   Afkjso;Z    Mgijadp Psivjaok -MCP=
 *
 *   Ogujk;Z     Isrtogujk © 2008 Mgijadp Camd; Psivjaokw NeArreIsmr(JSL:)
 *
 *     Kjg; rosuoam g; yodd ;syk,aodZ tsf ial odhg;kognfkd gk alh[so
 *     mshgyt gk flhdo kjd kdom; sy kjd ULF Udldoap Rfnpgi Pgidl;d a;
 *     rfnpg;jdh nt kjd Yodd :syk,aod Ysflhakgslw dgkjdo .do;gsl 3 sy
 *     kjd Pgidl;dw so (ak tsfo srkgsl) alt pakdo .do;gsle
 *
 *     Kjg; rosuoam g; hg;kognfkdh gl kjd jsrd kjak gk ,gpp nd f;dyfpw
 *     nfk <GKJSFK ALT <AOOALKTz ,gkjsfk d.dl kjd gmrpgdh ,aooalkt sy
 *     MDOIJALKANGPGKT so YGKLD:: YSO A RAOKGIFPAO RFORS:De  :dd kjd
 *     ULF Udldoap Rfnpgi Pgidl;d yso msod hdkagp;e
 *
 *     Tsf ;jsfph ja.d odidg.dh a isrt sy kjd ULF Udldoap Rfnpgi
 *     Pgidl;d apslu ,gkj kjg; rosuoame  Gy lskw ;dd
 *     WjkkrZ[[,,,eulfesou[pgidl;d;[Ee
 *
 *   RFORS:DZ
 *
 *     Ipa;; ks hdishd[dlishd ;koglu; f;glu Qh.soav ishdQe
 *     :dd magl ipa;; hsifmdlkakgsl
 *
 *   JG:KSOTZ
 *
 *   MCP20080721 ' Iodakdhe
 *   MCP20080722 ' Ahhdh IPG alh UFG (;drdoakd ipa;;d;)e
 *   MCP20080723 ' :,ar kjd marrglu; '' Gqm ;s islyf;dh!
 *   MCP20080819 ' OdyaiksoZ odlamd raivaud
 *                  yosm mcpeh.soishd ks ldke;yeh.soishd
 *               ' Ygb koal;ishd nfu yso ijao; lsk gl kjd mar; 
 *                 (deue FKY'8)
 *   MCP20080820 ' Ishd kght '' kot ks ygk ks 80 ispfml;
 *   MCP20080909 ' Odms.d pslu ;ksot yosm ca.ahsi '' G ja.d a ODAHMD
 *   MCP20080910 ' F;d al dlfm ks odrod;dlk H.soishd ishdi srdoakgsl (DLI[HDI)
 *               ' Ygb kd;k ishd gl magl()
 *
 *[


raivaud ldke;yeh.soishdz

gmrsok ca.aefkgpeMarz
gmrsok ca.aefkgpeJa;jMarz
gmrsok ;kakgi ca.aepalue:t;kdmesfkz

[**
 * Ipa;; ks koal;ishd a ;koglu a; gy dlkdodh nt vdtiar sl a h.soav
 * vdtnsaoh kjak g; maovdh ,gkj X<DOKT vdtiar;e Ial koal;pakd gl
 * dgkjdo hgodikgsl nt srkgslap ;,gkijeWr[E
 *
 * WfEF;d ia;dW[fEZ Vddr kjg; rosuoam sl a F:N mdmsote Gy Gqm sl a
 * X<DOKT vdtnsaoh alh G lddh ks vls, mt odap h.soishd Ussupd
 * ra;;,sohw rpfu gl kjd F:N mdmsotw ;kaok kjd rosuoamw ktrd gl kjd
 * ra;;,soh Gq.d odmdmndodh (Q,amrayofgkQ ;at) alh kjdl isrt[ra;kd kjd
 * od;fpkglu h.soishd (QwampafruitQ) glks kjd nos,;doeWr[E
 * 
 * @afkjso mgvd
 *[
rfnpgi ipa;; H.soishd _

    [** Kjg; mar; yosm X<DOKT vdtiar ks H.soav vdt .apfd *[
    rog.akd ;kakgi yglap Ja;jMarWIjaoaikdowIjaoaikdoE h.HdiMar 
            ] ld, Ja;jMarWIjaoaikdowIjaoaikdoE(33*2)z
    [** Kjg; mar; yosm H.soav vdt .apfd ks X<DOKT vdtiar *[
    rog.akd ;kakgi yglap Ja;jMarWIjaoaikdowIjaoaikdoE h.DliMar 
            ] ld, Ja;jMarWIjaoaikdowIjaoaikdoE(33*2)z

    [** dlfmdoakd; kjd H.soishd ishdi srdoakgsl;e
     *  H.soishd ial Dlishd so Hdishd hdrdlhglu frsl kjd dlfmdoakgsl
     *  ra;;dh ks gkq; WishdEkoal;ishd(:kogluw Ishdi)W[ishdE mdkjsh
     *[
    rfnpgi ;kakgi dlfm Ishdi _
        [** Glhgiakd; dlishglu srdoakgsl yso H.soishd *[
        DLISHDw 
        [** Glhgiakd; hdishglu srdoakgsl yso H.soishd *[
        HDISHD
    +z

    ;kakgi _ [[ marq; vdtiar 'E  vdt.apfd yso H.soav vdt marrglu (hdishglu)
        h.HdiMarerfk(q"qw q_q)z h.HdiMarerfk(q}qw q+q)z
        h.HdiMarerfk(q'qw q-q)z h.HdiMarerfk(q]qw q=q)z

        h.HdiMarerfk(qXqw qQq)z h.HdiMarerfk(q<qw qWq)z h.HdiMarerfk(qDqw qEq)z
        h.HdiMarerfk(qxqwq\qq)z h.HdiMarerfk(q,qw qwq)z h.HdiMarerfk(qdqw qeq)z

        h.HdiMarerfk(qOqw qRq)z h.HdiMarerfk(qKqw qTq)z h.HdiMarerfk(qTqw qYq)z
        h.HdiMarerfk(qoqw qrq)z h.HdiMarerfk(qkqw qtq)z h.HdiMarerfk(qtqw qyq)z

        h.HdiMarerfk(qFqw qUq)z h.HdiMarerfk(qGqw qIq)z h.HdiMarerfk(qSqw qOq)z
        h.HdiMarerfk(qfqw quq)z h.HdiMarerfk(qgqw qiq)z h.HdiMarerfk(qsqw qoq)z

        h.HdiMarerfk(qRqw qPq)z h.HdiMarerfk(q_qw q{q)z h.HdiMarerfk(q+qw q}q)z
        h.HdiMarerfk(qrqw qpq)z h.HdiMarerfk(q-qw q[q)z h.HdiMarerfk(q=qw q]q)z

                                h.HdiMarerfk(q:qw qSq)z h.HdiMarerfk(qHqw qDq)z
                                h.HdiMarerfk(q;qw qsq)z h.HdiMarerfk(qhqw qdq)z

        h.HdiMarerfk(qYqw qFq)z h.HdiMarerfk(qUqw qGq)z h.HdiMarerfk(qJqw qHq)z
        h.HdiMarerfk(qyqw qfq)z h.HdiMarerfk(quqw qgq)z h.HdiMarerfk(qjqw qhq)z

        h.HdiMarerfk(qCqw qJq)z h.HdiMarerfk(qVqw qKq)z h.HdiMarerfk(qPqw qLq)z
        h.HdiMarerfk(qcqw qjq)z h.HdiMarerfk(qvqw qkq)z h.HdiMarerfk(qpqw qlq)z

        h.HdiMarerfk(qZqw q:q)z h.HdiMarerfk(qQqw q"q)z h.HdiMarerfk(q?qw qZq)z
        h.HdiMarerfk(qzqw q;q)z h.HdiMarerfk(q\qqwq'q)z h.HdiMarerfk(q/qw qzq)z

        h.HdiMarerfk(qBqw qXq)z h.HdiMarerfk(qIqw qCq)z h.HdiMarerfk(q>qw qVq)z
        h.HdiMarerfk(qbqw qxq)z h.HdiMarerfk(qiqw qcq)z h.HdiMarerfk(q.qw qvq)z

        h.HdiMarerfk(qNqw qBq)z h.HdiMarerfk(qLqw qNq)z
        h.HdiMarerfk(qnqw qbq)z h.HdiMarerfk(qlqw qnq)z

        h.HdiMarerfk(qWqw q<q)z h.HdiMarerfk(qEqw q>q)z h.HdiMarerfk(q{qw q?q)z
        h.HdiMarerfk(qwqw q,q)z h.HdiMarerfk(qeqw q.q)z h.HdiMarerfk(q[qw q/q)z
    +
    
    ;kakgi _ [[ marq; vdt.apfd 'E  vdtiar yso H.soav vdt marrglu (dlishglu)
        [[ Kjg; g; cf;k a isrt sy kjd dlishglu mar ,gkj Vdt alh >apfd ;,arrdh
        yso (MareDlkotWIjaoaikdowIjaoaikdoE d Z h.HdiMaredlkot:dk()) _
            h.DliMarerfk(deudk>apfd()w deudkVdt())z
        +
    +
    
    [**
     * hdishd; so dlishd; kjd glrfk :koglu ks[yosm H> dlishglue
     * 
     * @raoam itrjdo :koglu ks koal;ishd
     * @raoam hgodikgsl f;d kjd dlfm ks glhgiakd dlishd so hdishd
     * @odkfol Kjd :koglu koal;ishdh ks[yosm H> dlishglu a; odxfd;kdhe
     *[
    rfnpgi ;kakgi :koglu koal;ishd(:koglu itrjdow Ishdi hgodikgsl) _
        Mar ishdi ] (IshdieDLISHD ]] hgodikgsl) { h.DliMar Z h.HdiMarz
       
        gy ((lfpp ]] itrjdo) || (lfpp ]] hgodikgsl)) _
            odkfol(lfpp)z
        +
        
        :kogluNfgphdo md;;aud ] ld, :kogluNfgphdo(itrjdo)z
        yso (glk b ] 0z b W md;;audepdlukj()z b}}) _
            gy (ishdieislkagl;Vdt(itrjdoeijaoAk(b))) _
                md;;audeodrpaid(bw b } 1w
                                ishdieudk(itrjdoeijaoAk(b))eks:koglu())z
            +
        +

        odkfol md;;audeks:koglu()z
    +
    
    [**
     * Hdishd; kjd glrfk :koglue Kjg; g; ;fuuao yso koal;ishd(itrjdow
     * IshdieHDISHD)w so a ;tlsltm yso koal;ishd(itrjdo)e
     * 
     * @raoam itrjdo :koglu ks hdishd
     * @odkfol kjd :koglu hdishdh yosm H> dlishglu
     *[
    rfnpgi ;kakgi :koglu hdishd(:koglu itrjdo) _
        odkfol koal;ishd (itrjdo)z
    +
    
    [**
     * Dlishd; kjd glrfk :koglue Kjg; g; ;fuuao yso koal;ishd(itrjdow
     * IshdieDLISHD)e
     * 
     * @raoam md;;aud :koglu ks dlishd
     * @odkfol kjd :koglu dlishdh gl H> dlishglu
     *[
    rfnpgi ;kakgi :koglu dlishd(:koglu md;;aud) _
        odkfol koal;ishd (md;;audw IshdieDLISHD)z
    +

    [**
     * Hdishd; kjd glrfk :koglue Kjg; g; ;fuuao yso koal;ishd(itrjdow
     * IshdieHDISHD)e  Kjg; gmrpdmdlk; a hdyafpk srdoakgsl ,jdl lsld g;
     * ;rdigygdh (mavd; kjd hgodikgsl raoam srkgslap)e
     * 
     * @raoam itrjdo :koglu ks hdishd
     * @odkfol :koglu hdishdh yosm H> dlishglu
     *[
    rfnpgi ;kakgi :koglu koal;ishd(:koglu itrjdo) _
        odkfol koal;ishd (itrjdow IshdieHDISHD)z 
    +
    
    [**
     * F;dh a; a kd;k osfkgld slpte Ja.d a odap f;do glkdoyaid hog.d
     * kjg; ipa;; gl;kdah sy iappglu magl()e
     * 
     * @raoam aou; kjd ismmalh pgld aoufmdlk;
     *[
    rfnpgi ;kakgi .sgh magl(:koglu-= aou;) _
        [[ kd;k ishd
        sfkeroglkpl(QH.soav\k\kX<DOKTQ)z
        sfkeroglkpl(Q''''''\k\k''''''Q)z
        sfkeroglkpl(hdishd(Q~!@#$%^&*()"}Q) }Q\kQ} dlishd(Q~!@#$%^&*()_+Q))z
        sfkeroglkpl(hdishd(Q`1234567890']Q) }Q\kQ} dlishd(Q`1234567890-=\lQ))z
                
        sfkeroglkpl(hdishd(QX<DOKTFGSR_+|Q) }Q\kQ} dlishd(Q\QWERTYUIOP{}|Q))z
        sfkeroglkpl(hdishd(Qx,doktfgsr-=\\Q)}Q\kQ} dlishd(Qqwertyuiop[]\\\lQ))z
        
        sfkeroglkpl(hdishd(Q A:HYUJCVPZ\QQ) }Q\kQ} dlishd(Q ASDFGHJKL:"Q))z
        sfkeroglkpl(hdishd(Q a;hyujcvpzqQ)  }Q\kQ} dlishd(Q asdfghjkl;'\lQ))z
        
        sfkeroglkpl(hdishd(Q  ?BI>NLMWE{Q)  }Q\kQ} dlishd(Q  ZXCVBNM<>?Q))z
        sfkeroglkpl(hdishd(Q  /bi.nlmwe[Q)  }Q\kQ} dlishd(Q  zxcvbnm,./Q))z

        sfkeroglkpl(Qyspps,glu kd;k; ;jsfph app nd kofdQ)z
        sfkeroglk(hdishd(Q~!@#$%^&*()"}Q)edxfap;(Q~!@#$%^&*()_+Q) } Q Q)z
        sfkeroglk(hdishd(Q`1234567890']Q)edxfap;(Q`1234567890-=Q) } Q Q)z
        sfkeroglk(hdishd(QX<DOKTFGSR_+|Q)edxfap;(Q\QWERTYUIOP{}|Q) } Q Q)z
        sfkeroglk(hdishd(Qx,doktfgsr-=\\Q)edxfap;(Qqwertyuiop[]\\Q) } Q Q)z
        sfkeroglk(hdishd(QA:HYUJCVPZ\QQ)edxfap;(QASDFGHJKL:"Q) } Q Q)z
        sfkeroglk(hdishd(Qa;hyujcvpzqQ)edxfap;(Qasdfghjkl;'Q) } Q Q)z
        sfkeroglk(hdishd(Q?BI>NLMWE{Q)edxfap;(QZXCVBNM<>?Q) } Q Q)z
        sfkeroglkpl(hdishd(Q/bi.nlmwe[Q)edxfap;(Qzxcvbnm,./Q))z
        
        sfkeroglk(dlishd(Q~!@#$%^&*()_+Q)edxfap;(Q~!@#$%^&*()"}Q) } Q Q)z
        sfkeroglk(dlishd(Q`1234567890-=Q)edxfap;(Q`1234567890']Q) } Q Q)z
        sfkeroglk(dlishd(Q\QWERTYUIOP{}|Q)edxfap;(QX<DOKTFGSR_+|Q) } Q Q)z
        sfkeroglk(dlishd(Qqwertyuiop[]\\Q)edxfap;(Qx,doktfgsr-=\\Q) } Q Q)z
        sfkeroglk(dlishd(QASDFGHJKL:"Q)edxfap;(QA:HYUJCVPZ\QQ) } Q Q)z
        sfkeroglk(dlishd(Qasdfghjkl;'Q)edxfap;(Qa;hyujcvpzqQ) } Q Q)z
        sfkeroglk(dlishd(QZXCVBNM<>?Q)edxfap;(Q?BI>NLMWE{Q) } Q Q)z
        sfkeroglkpl(dlishd(Qzxcvbnm,./Q)edxfap;(Q/bi.nlmwe[Q))z
    +
+
[* ***************************************************************************
 *
 *   YgpdZ       H.soishdeca.a
 *   PalufaudZ   Ca.a 1e5
  *   RpakysomZ   Ca.a 5e0 (QKgudoQ) so ld,do
 *   S:Z         l[a
 *   Afkjso;Z    Mgijadp Psivjaok -MCP=
 *
 *   Ogujk;Z     Isrtogujk © 2008 Mgijadp Camd; Psivjaokw NeArreIsmr(JSL:)
 *
 *     Kjg; rosuoam g; yodd ;syk,aodZ tsf ial odhg;kognfkd gk alh[so
 *     mshgyt gk flhdo kjd kdom; sy kjd ULF Udldoap Rfnpgi Pgidl;d a;
 *     rfnpg;jdh nt kjd Yodd :syk,aod Ysflhakgslw dgkjdo .do;gsl 3 sy
 *     kjd Pgidl;dw so (ak tsfo srkgsl) alt pakdo .do;gsle
 *
 *     Kjg; rosuoam g; hg;kognfkdh gl kjd jsrd kjak gk ,gpp nd f;dyfpw
 *     nfk <GKJSFK ALT <AOOALKTz ,gkjsfk d.dl kjd gmrpgdh ,aooalkt sy
 *     MDOIJALKANGPGKT so YGKLD:: YSO A RAOKGIFPAO RFORS:De  :dd kjd
 *     ULF Udldoap Rfnpgi Pgidl;d yso msod hdkagp;e
 *
 *     Tsf ;jsfph ja.d odidg.dh a isrt sy kjd ULF Udldoap Rfnpgi
 *     Pgidl;d apslu ,gkj kjg; rosuoame  Gy lskw ;dd
 *     WjkkrZ[[,,,eulfesou[pgidl;d;[Ee
 *
 *   RFORS:DZ
 *
 *     Ipa;; ks hdishd[dlishd ;koglu; f;glu Qh.soav ishdQe
 *     :dd magl ipa;; hsifmdlkakgsl
 *
 *   JG:KSOTZ
 *
 *   MCP20080721 ' Iodakdhe
 *   MCP20080722 ' Ahhdh IPG alh UFG (;drdoakd ipa;;d;)e
 *   MCP20080723 ' :,ar kjd marrglu; '' Gqm ;s islyf;dh!
 *   MCP20080819 ' OdyaiksoZ odlamd raivaud
 *                  yosm mcpeh.soishd ks ldke;yeh.soishd
 *               ' Ygb koal;ishd nfu yso ijao; lsk gl kjd mar; 
 *                 (deue FKY'8)
 *   MCP20080820 ' Ishd kght '' kot ks ygk ks 80 ispfml;
 *   MCP20080909 ' Odms.d pslu ;ksot yosm ca.ahsi '' G ja.d a ODAHMD
 *   MCP20080910 ' F;d al dlfm ks odrod;dlk H.soishd ishdi srdoakgsl (DLI[HDI)
 *               ' Ygb kd;k ishd gl magl()
 *
 *[


raivaud ldke;yeh.soishdz

gmrsok ca.aefkgpeMarz
gmrsok ca.aefkgpeJa;jMarz
gmrsok ;kakgi ca.aepalue:t;kdmesfkz

[**
 * Ipa;; ks koal;ishd a ;koglu a; gy dlkdodh nt vdtiar sl a h.soav
 * vdtnsaoh kjak g; maovdh ,gkj X<DOKT vdtiar;e Ial koal;pakd gl
 * dgkjdo hgodikgsl nt srkgslap ;,gkijeWr[E
 *
 * WfEF;d ia;dW[fEZ Vddr kjg; rosuoam sl a F:N mdmsote Gy Gqm sl a
 * X<DOKT vdtnsaoh alh G lddh ks vls, mt odap h.soishd Ussupd
 * ra;;,sohw rpfu gl kjd F:N mdmsotw ;kaok kjd rosuoamw ktrd gl kjd
 * ra;;,soh Gq.d odmdmndodh (Q,amrayofgkQ ;at) alh kjdl isrt[ra;kd kjd
 * od;fpkglu h.soishd (QwampafruitQ) glks kjd nos,;doeWr[E
 * 
 * @afkjso mgvd
 *[
rfnpgi ipa;; H.soishd _

    [** Kjg; mar; yosm X<DOKT vdtiar ks H.soav vdt .apfd *[
    rog.akd ;kakgi yglap Ja;jMarWIjaoaikdowIjaoaikdoE h.HdiMar 
            ] ld, Ja;jMarWIjaoaikdowIjaoaikdoE(33*2)z
    [** Kjg; mar; yosm H.soav vdt .apfd ks X<DOKT vdtiar *[
    rog.akd ;kakgi yglap Ja;jMarWIjaoaikdowIjaoaikdoE h.DliMar 
            ] ld, Ja;jMarWIjaoaikdowIjaoaikdoE(33*2)z

    [** dlfmdoakd; kjd H.soishd ishdi srdoakgsl;e
     *  H.soishd ial Dlishd so Hdishd hdrdlhglu frsl kjd dlfmdoakgsl
     *  ra;;dh ks gkq; WishdEkoal;ishd(:kogluw Ishdi)W[ishdE mdkjsh
     *[
    rfnpgi ;kakgi dlfm Ishdi _
        [** Glhgiakd; dlishglu srdoakgsl yso H.soishd *[
        DLISHDw 
        [** Glhgiakd; hdishglu srdoakgsl yso H.soishd *[
        HDISHD
    +z

    ;kakgi _ [[ marq; vdtiar 'E  vdt.apfd yso H.soav vdt marrglu (hdishglu)
        h.HdiMarerfk(q"qw q_q)z h.HdiMarerfk(q}qw q+q)z
        h.HdiMarerfk(q'qw q-q)z h.HdiMarerfk(q]qw q=q)z

        h.HdiMarerfk(qXqw qQq)z h.HdiMarerfk(q<qw qWq)z h.HdiMarerfk(qDqw qEq)z
        h.HdiMarerfk(qxqwq\qq)z h.HdiMarerfk(q,qw qwq)z h.HdiMarerfk(qdqw qeq)z

        h.HdiMarerfk(qOqw qRq)z h.HdiMarerfk(qKqw qTq)z h.HdiMarerfk(qTqw qYq)z
        h.HdiMarerfk(qoqw qrq)z h.HdiMarerfk(qkqw qtq)z h.HdiMarerfk(qtqw qyq)z

        h.HdiMarerfk(qFqw qUq)z h.HdiMarerfk(qGqw qIq)z h.HdiMarerfk(qSqw qOq)z
        h.HdiMarerfk(qfqw quq)z h.HdiMarerfk(qgqw qiq)z h.HdiMarerfk(qsqw qoq)z

        h.HdiMarerfk(qRqw qPq)z h.HdiMarerfk(q_qw q{q)z h.HdiMarerfk(q+qw q}q)z
        h.HdiMarerfk(qrqw qpq)z h.HdiMarerfk(q-qw q[q)z h.HdiMarerfk(q=qw q]q)z

                                h.HdiMarerfk(q:qw qSq)z h.HdiMarerfk(qHqw qDq)z
                                h.HdiMarerfk(q;qw qsq)z h.HdiMarerfk(qhqw qdq)z

        h.HdiMarerfk(qYqw qFq)z h.HdiMarerfk(qUqw qGq)z h.HdiMarerfk(qJqw qHq)z
        h.HdiMarerfk(qyqw qfq)z h.HdiMarerfk(quqw qgq)z h.HdiMarerfk(qjqw qhq)z

        h.HdiMarerfk(qCqw qJq)z h.HdiMarerfk(qVqw qKq)z h.HdiMarerfk(qPqw qLq)z
        h.HdiMarerfk(qcqw qjq)z h.HdiMarerfk(qvqw qkq)z h.HdiMarerfk(qpqw qlq)z

        h.HdiMarerfk(qZqw q:q)z h.HdiMarerfk(qQqw q"q)z h.HdiMarerfk(q?qw qZq)z
        h.HdiMarerfk(qzqw q;q)z h.HdiMarerfk(q\qqwq'q)z h.HdiMarerfk(q/qw qzq)z

        h.HdiMarerfk(qBqw qXq)z h.HdiMarerfk(qIqw qCq)z h.HdiMarerfk(q>qw qVq)z
        h.HdiMarerfk(qbqw qxq)z h.HdiMarerfk(qiqw qcq)z h.HdiMarerfk(q.qw qvq)z

        h.HdiMarerfk(qNqw qBq)z h.HdiMarerfk(qLqw qNq)z
        h.HdiMarerfk(qnqw qbq)z h.HdiMarerfk(qlqw qnq)z

        h.HdiMarerfk(qWqw q<q)z h.HdiMarerfk(qEqw q>q)z h.HdiMarerfk(q{qw q?q)z
        h.HdiMarerfk(qwqw q,q)z h.HdiMarerfk(qeqw q.q)z h.HdiMarerfk(q[qw q/q)z
    +
    
    ;kakgi _ [[ marq; vdt.apfd 'E  vdtiar yso H.soav vdt marrglu (dlishglu)
        [[ Kjg; g; cf;k a isrt sy kjd dlishglu mar ,gkj Vdt alh >apfd ;,arrdh
        yso (MareDlkotWIjaoaikdowIjaoaikdoE d Z h.HdiMaredlkot:dk()) _
            h.DliMarerfk(deudk>apfd()w deudkVdt())z
        +
    +
    
    [**
     * hdishd; so dlishd; kjd glrfk :koglu ks[yosm H> dlishglue
     * 
     * @raoam itrjdo :koglu ks koal;ishd
     * @raoam hgodikgsl f;d kjd dlfm ks glhgiakd dlishd so hdishd
     * @odkfol Kjd :koglu koal;ishdh ks[yosm H> dlishglu a; odxfd;kdhe
     *[
    rfnpgi ;kakgi :koglu koal;ishd(:koglu itrjdow Ishdi hgodikgsl) _
        Mar ishdi ] (IshdieDLISHD ]] hgodikgsl) { h.DliMar Z h.HdiMarz
       
        gy ((lfpp ]] itrjdo) || (lfpp ]] hgodikgsl)) _
            odkfol(lfpp)z
        +
        
        :kogluNfgphdo md;;aud ] ld, :kogluNfgphdo(itrjdo)z
        yso (glk b ] 0z b W md;;audepdlukj()z b}}) _
            gy (ishdieislkagl;Vdt(itrjdoeijaoAk(b))) _
                md;;audeodrpaid(bw b } 1w
                                ishdieudk(itrjdoeijaoAk(b))eks:koglu())z
            +
        +

        odkfol md;;audeks:koglu()z
    +
    
    [**
     * Hdishd; kjd glrfk :koglue Kjg; g; ;fuuao yso koal;ishd(itrjdow
     * IshdieHDISHD)w so a ;tlsltm yso koal;ishd(itrjdo)e
     * 
     * @raoam itrjdo :koglu ks hdishd
     * @odkfol kjd :koglu hdishdh yosm H> dlishglu
     *[
    rfnpgi ;kakgi :koglu hdishd(:koglu itrjdo) _
        odkfol koal;ishd (itrjdo)z
    +
    
    [**
     * Dlishd; kjd glrfk :koglue Kjg; g; ;fuuao yso koal;ishd(itrjdow
     * IshdieDLISHD)e
     * 
     * @raoam md;;aud :koglu ks dlishd
     * @odkfol kjd :koglu dlishdh gl H> dlishglu
     *[
    rfnpgi ;kakgi :koglu dlishd(:koglu md;;aud) _
        odkfol koal;ishd (md;;audw IshdieDLISHD)z
    +

    [**
     * Hdishd; kjd glrfk :koglue Kjg; g; ;fuuao yso koal;ishd(itrjdow
     * IshdieHDISHD)e  Kjg; gmrpdmdlk; a hdyafpk srdoakgsl ,jdl lsld g;
     * ;rdigygdh (mavd; kjd hgodikgsl raoam srkgslap)e
     * 
     * @raoam itrjdo :koglu ks hdishd
     * @odkfol :koglu hdishdh yosm H> dlishglu
     *[
    rfnpgi ;kakgi :koglu koal;ishd(:koglu itrjdo) _
        odkfol koal;ishd (itrjdow IshdieHDISHD)z 
    +
    
    [**
     * F;dh a; a kd;k osfkgld slpte Ja.d a odap f;do glkdoyaid hog.d
     * kjg; ipa;; gl;kdah sy iappglu magl()e
     * 
     * @raoam aou; kjd ismmalh pgld aoufmdlk;
     *[
    rfnpgi ;kakgi .sgh magl(:koglu-= aou;) _
        [[ kd;k ishd
        sfkeroglkpl(QH.soav\k\kX<DOKTQ)z
        sfkeroglkpl(Q''''''\k\k''''''Q)z
        sfkeroglkpl(hdishd(Q~!@#$%^&*()"}Q) }Q\kQ} dlishd(Q~!@#$%^&*()_+Q))z
        sfkeroglkpl(hdishd(Q`1234567890']Q) }Q\kQ} dlishd(Q`1234567890-=\lQ))z
                
        sfkeroglkpl(hdishd(QX<DOKTFGSR_+|Q) }Q\kQ} dlishd(Q\QWERTYUIOP{}|Q))z
        sfkeroglkpl(hdishd(Qx,doktfgsr-=\\Q)}Q\kQ} dlishd(Qqwertyuiop[]\\\lQ))z
        
        sfkeroglkpl(hdishd(Q A:HYUJCVPZ\QQ) }Q\kQ} dlishd(Q ASDFGHJKL:"Q))z
        sfkeroglkpl(hdishd(Q a;hyujcvpzqQ)  }Q\kQ} dlishd(Q asdfghjkl;'\lQ))z
        
        sfkeroglkpl(hdishd(Q  ?BI>NLMWE{Q)  }Q\kQ} dlishd(Q  ZXCVBNM<>?Q))z
        sfkeroglkpl(hdishd(Q  /bi.nlmwe[Q)  }Q\kQ} dlishd(Q  zxcvbnm,./Q))z

        sfkeroglkpl(Qyspps,glu kd;k; ;jsfph app nd kofdQ)z
        sfkeroglk(hdishd(Q~!@#$%^&*()"}Q)edxfap;(Q~!@#$%^&*()_+Q) } Q Q)z
        sfkeroglk(hdishd(Q`1234567890']Q)edxfap;(Q`1234567890-=Q) } Q Q)z
        sfkeroglk(hdishd(QX<DOKTFGSR_+|Q)edxfap;(Q\QWERTYUIOP{}|Q) } Q Q)z
        sfkeroglk(hdishd(Qx,doktfgsr-=\\Q)edxfap;(Qqwertyuiop[]\\Q) } Q Q)z
        sfkeroglk(hdishd(QA:HYUJCVPZ\QQ)edxfap;(QASDFGHJKL:"Q) } Q Q)z
        sfkeroglk(hdishd(Qa;hyujcvpzqQ)edxfap;(Qasdfghjkl;'Q) } Q Q)z
        sfkeroglk(hdishd(Q?BI>NLMWE{Q)edxfap;(QZXCVBNM<>?Q) } Q Q)z
        sfkeroglkpl(hdishd(Q/bi.nlmwe[Q)edxfap;(Qzxcvbnm,./Q))z
        
        sfkeroglk(dlishd(Q~!@#$%^&*()_+Q)edxfap;(Q~!@#$%^&*()"}Q) } Q Q)z
        sfkeroglk(dlishd(Q`1234567890-=Q)edxfap;(Q`1234567890']Q) } Q Q)z
        sfkeroglk(dlishd(Q\QWERTYUIOP{}|Q)edxfap;(QX<DOKTFGSR_+|Q) } Q Q)z
        sfkeroglk(dlishd(Qqwertyuiop[]\\Q)edxfap;(Qx,doktfgsr-=\\Q) } Q Q)z
        sfkeroglk(dlishd(QASDFGHJKL:"Q)edxfap;(QA:HYUJCVPZ\QQ) } Q Q)z
        sfkeroglk(dlishd(Qasdfghjkl;'Q)edxfap;(Qa;hyujcvpzqQ) } Q Q)z
        sfkeroglk(dlishd(QZXCVBNM<>?Q)edxfap;(Q?BI>NLMWE{Q) } Q Q)z
        sfkeroglkpl(dlishd(Qzxcvbnm,./Q)edxfap;(Q/bi.nlmwe[Q))z
    +
+
